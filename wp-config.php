<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'projetos_amandakaroline_loja');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'root');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');


// define('WP_HOME','http://localhost:8888/projetos/amandakaroline_loja/');
// define('WP_SITEURL','http://localhost:8888/projetos/amandakaroline_loja/');
// define('RELOCATE',true);
/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$^-{Z= ,ra]6f6)BVWxfgSrxm<:ToO$vH?F89LO3e*a*7j_N1-p*o/{KsF4G~HM>');
define('SECURE_AUTH_KEY',  'Sa.VV^c {wz: 9~gB_vrNqwi#J3{U~P08El&ey)X_:DmAZIlCg-(Dw$?:ED^Ucq>');
define('LOGGED_IN_KEY',    '>Uwo+c R_A=1|dsQT8@6x[M7vJ2H(][wcG|{T,KLJeT*fhL-~Ed= 4g4=7cl>DP^');
define('NONCE_KEY',        '$TZUf}|*0K)G`~{>?=Y1so3py7xy@EjX[Hk,o1]|=fUy5x)SC.&f^*BOCEqr%Jj]');
define('AUTH_SALT',        '@Ph##np iOwE?^ E@.=0OVoB|A;l|os^Ay2}iUnN$gCAuL6H>.CWgynKwV>unH}.');
define('SECURE_AUTH_SALT', 'pgoK0;G*&@D]Den&7Vcgylz~w)Jsuz8d~HnJs_1HTx],lp)#c*?$igS:P>DqR+3h');
define('LOGGED_IN_SALT',   '<)rdS9K1VI{(bI)~qpH<^M=z`]mp)}CM!|Jb40cR[L~j}aV-b5^T5|?jh`=CuqL7');
define('NONCE_SALT',       'w)yHxh&lg,Cl.5M>]*{.a#!p{4A`ySlr|@TiP6Qhh%::*9@m.hoYO; 5wy-HHF/0');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'ak_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */
define( 'ALLOW_UNFILTERED_UPLOADS', true );

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
