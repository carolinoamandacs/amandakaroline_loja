<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package AKBLACK
 */

get_header();

$cate = get_queried_object();
$cateID = $cate->term_id;
if ($cateID) {
	$imagemCategoria = z_taxonomy_image_url($cateID);
}
?>

<div class="categoria" >
	<div class="container">
		<?php if ($cateID):?>
		<section class="bannerDestaque pgCategoriaBanner">
			<h6 class="hidden"> <?php echo $cate->name; ?></h6>
			<?php if ($imagemCategoria):?>
			<a href="">
				<img src="<?php echo $imagemCategoria ?> " alt="<?php echo $cate->name; ?>">
		    </a>
		<?php endif; ?>
		</section>
	<?php endif; ?>
		<div class="row">

			<?php echo get_template_part( 'views/parts/sideBar_categoria', 'sideBar_categoria' ); ?>

			<div class="col-sm-10">
				
				<section class="spotsProdutos">
					<h6 class="hidden">Produtos</h6>

					<?php 
						while ( have_posts() ) : the_post();

							get_template_part( 'template-parts/content', 'page' );

							if ( comments_open() || get_comments_number() ) :
								comments_template();
						endif; endwhile; // End of the loop.
					?>
				</section>
			</div>
		</div>
	</div>
</div>
	
<?php
get_footer();
