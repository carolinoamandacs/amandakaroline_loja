$(function(){


    /**********************************************************************
        SCRIPTS DO CARROSSEIS
    **********************************************************************/
  $(document).ready(function() {
    $('.carrosselProdutosSlick1').slick({
        slidesToShow: 4,
        lazyLoad: 'ondemand',
        dots: false,
        touchMove:false,
        centerMode: false,
        focusOnSelect: false,
        autoplay: true,
        infinite:true,
        speed: 1000,
        autoplaySpeed: 3000,
        slickNext:true,
        slickPrev:true,
         responsive: [
        {
          breakpoint: 1920,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 2,
            infinite: true,
          }
        },
        {
          breakpoint: 1280,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 4
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 4
          }
        }
      ]
    });

    $('.carrosselProdutosSlick2').slick({
        slidesToShow: 4,
        lazyLoad: 'ondemand',
        dots: false,
        touchMove:false,
        centerMode: false,
        focusOnSelect: false,
        autoplay: true,
        infinite:true,
        speed: 1000,
        autoplaySpeed: 2500,
        slickNext:true,
        slickPrev:true,
         responsive: [
        {
          breakpoint: 1920,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 2,
            infinite: true,
          }
        },
        {
          breakpoint: 1280,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 14
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 14
          }
        }
      ]
    });

    $('.carrosselProdutosSlick3').slick({
        slidesToShow: 4,
        lazyLoad: 'ondemand',
        dots: false,
        touchMove:false,
        centerMode: false,
        focusOnSelect: false,
        autoplay: true,
        infinite:true,
        speed: 1000,
        autoplaySpeed: 2250,
        slickNext:true,
        slickPrev:true,
         responsive: [
        {
          breakpoint: 1920,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 2,
            infinite: true,
          }
        },
        {
          breakpoint: 1280,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 4
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 4
          }
        }
      ]
    });
    $('.carrosselProdutosSlick4').slick({
        slidesToShow: 4,
        lazyLoad: 'ondemand',
        dots: false,
        touchMove:false,
        centerMode: false,
        focusOnSelect: false,
        autoplay: true,
        infinite:true,
        speed: 1000,
        autoplaySpeed: 2000,
        slickNext:true,
        slickPrev:true,
         responsive: [
        {
          breakpoint: 1920,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 2,
            infinite: true,
          }
        },
        {
          breakpoint: 1280,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 4
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 4
          }
        }
      ]
    });

    $('.carrosselCategoria').slick({
        slidesToShow: 7,
        lazyLoad: 'ondemand',
        dots: false,
        touchMove:false,
        centerMode: false,
        focusOnSelect: false,
        autoplay: true,
        infinite:true,
        speed: 1000,
        autoplaySpeed: 2000,
         responsive: [
        {
          breakpoint: 1920,
          settings: {
            slidesToShow: 7,
            slidesToScroll: 1,
            infinite: true,
          }
        },
        {
          breakpoint: 1280,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1
          }
        }
      ]
    });

    $('.carrosselDestaque').slick({
        slidesToShow: 1,
        lazyLoad: 'ondemand',
        dots: true,
        touchMove:false,
        centerMode: false,
        focusOnSelect: false,
        autoplay: true,
        infinite:true,
        speed: 1000,
        autoplaySpeed: 2000,
        slickNext:true,
        slickPrev:true,
    });

    $('.carrosselParceiros').slick({
        slidesToShow: 3,
        lazyLoad: 'ondemand',
        dots: false,
        touchMove:false,
        centerMode: false,
        focusOnSelect: false,
        autoplay: true,
        infinite:true,
        speed: 1000,
        autoplaySpeed: 2000,
        slickNext:true,
        slickPrev:true,
         responsive: [
        {
          breakpoint: 1920,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
          }
        },
        {
          breakpoint: 1280,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });
           
  });

});