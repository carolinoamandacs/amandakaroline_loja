$(function(){


    $(window).load(function(){
        setTimeout(function(){
            $('.loadSite').addClass('removeLoaded');
            $('body').removeClass('scrollStop');
            setTimeout(function(){
                $('.menuMobile').slideDown();
            }, 1000);
        }, 1000);
    });

        $("#menuOpen").click(function(e) {
            $(".menuInstitucional").addClass("openMenumenuInstitucional");
            $('body').addClass('scrollStop');
        });
        $("#openPesquisa").click(function(e) {
            $(".formPesquisaMobile").slideDown("openMenumenuInstitucional");
            $(this).hide();
            $("#closePesquisa").show();
    
        });
        $("#closePesquisa").click(function(e) {
            $(".formPesquisaMobile").slideUp("openMenumenuInstitucional");
            $(this).hide();
            $("#openPesquisa").show();
    
        });
        $("#closeMenuInstitucional").click(function(e) {
            $(".menuInstitucional").removeClass("openMenumenuInstitucional");
            $('body').removeClass('scrollStop');
        });

        $("#menuHome").click(function(e) {
            $("#menuHomeInicial").show(500);
            $('body').addClass('scrollStop');
        });


        $(".mobile nav ul li a").click(function(e) {
            $("#menuHomeInicial nav").slideUp();
        });


    /*****************************************
        SCRIPTS SPOT DO PRODUTO
    *******************************************/
    $( "section.spotsProdutos ul li" ).mouseenter(function(e){
       let imgFoco = $(this).children().children().children("img").attr("data-imgFoco");
       $(this).children().children().children("img").attr("src",imgFoco);
    }).mouseleave(function(e){
        let imgPadrao = $(this).children().children().children("img").attr("data-imgPadrao");
        $(this).children().children().children("img").attr("src",imgPadrao);
    });


    $( "section.hovercarroseis .item .spot" ).mouseenter(function(e){
       let imgFoco = $(this).children().children().children("img").attr("data-imgFoco");
       $(this).children().children().children("img").attr("src",imgFoco);
    }).mouseleave(function(e){
        let imgPadrao = $(this).children().children().children("img").attr("data-imgPadrao");
        $(this).children().children().children("img").attr("src",imgPadrao);
    });


    $(".pgPrivacidade .secao-ajuda ul li .pergunta").click(function(e) {

        $(".pgPrivacidade .secao-ajuda ul li .pergunta").removeClass("ativo");

        $(this).addClass("ativo");

        $(".pgPrivacidade .secao-ajuda ul li .resposta").addClass("ativo");

        $(this).next().removeClass("ativo");

        $(".pgPrivacidade .secao-ajuda ul li").removeClass("ativo");

        $(this).parent().addClass("ativo");
    });

     $(".pg-produto .produto .galeria figure img").click(function(e){
        let url_imagem = $(this).attr("src");
        console.log(url_imagem);
        $('.pg-produto .produto .imgImagemDestacada img').attr("src",url_imagem);
    });

    $(".pg-produto .produto .galeriaResponsiva .carrosselGaleriaProdutos .item img").click(function(e){
        let url_imagem = $(this).attr("src");
        console.log(url_imagem);
        $('.pg-produto .produto .imgImagemDestacada img').attr("src",url_imagem);
    });
   

    $('.pg-produto .produto .descircaoProduto .breveDescricaoProduto a').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
        
    });

    $('.reload').click(function() {
                location.reload();
            });
            $('.btnComprarClique').click(function(e) {
                //e.preventDefault();
                 $(".loaderSucessoCarrinho").addClass("openAviso");
                var productId = $(this).attr('data-productId');
                addToCart(productId);
            return false;
            });
            function addToCart(product_id) {
                
                jQuery.ajax({
                    type: 'POST',
                    url: '/projetos/amandakaroline_loja/?post_type=product&add-to-cart='+product_id,
                    data: { 'product_id':  product_id,
                    'quantity': '1'},


                    success: function(response, textStatus, jqXHR){

                       
                       

                    },
                    // dataType: 'JSON'
                });
            }

     




});