$(function(){

  /**********************************************************************
      SCRIPTS DO CARROSSEIS
  **********************************************************************/
  $(document).ready(function() {

    $('.carrosselCategoria').slick({
      slidesToShow: 3,
      lazyLoad: 'ondemand',
      dots: false,
      touchMove:false,
      centerMode: false,
      focusOnSelect: false,
      autoplay: true,
      infinite:true,
      speed: 1000,
      autoplaySpeed: 2000,
    });

    $('.carrosselProdutosSlick1').slick({
      slidesToShow: 1,
      lazyLoad: 'ondemand',
      dots: false,
      touchMove:false,
      centerMode: false,
      focusOnSelect: false,
      autoplay: true,
      infinite:false,
      speed: 1000,
      autoplaySpeed: 3000,
      slickNext:true,
      slickPrev:true,
    });

    $('.carrosselProdutosSlick2').slick({
        slidesToShow: 1,
        lazyLoad: 'ondemand',
        dots: false,
        touchMove:false,
        centerMode: false,
        focusOnSelect: false,
        autoplay: true,
        infinite:false,
        speed: 1000,
        autoplaySpeed: 3000,
        slickNext:true,
        slickPrev:true,
    });

    $('.carrosselProdutosSlick3').slick({
      slidesToShow: 1,
      lazyLoad: 'ondemand',
      dots: false,
      touchMove:false,
      centerMode: false,
      focusOnSelect: false,
      autoplay: true,
      infinite:false,
      speed: 1000,
      autoplaySpeed: 3000,
      slickNext:true,
      slickPrev:true,
    });
    
    $('.carrosselProdutosSlick4').slick({
      slidesToShow: 1,
      lazyLoad: 'ondemand',
      dots: false,
      touchMove:false,
      centerMode: false,
      focusOnSelect: false,
      autoplay: true,
      infinite:false,
      speed: 1000,
      autoplaySpeed: 3000,
      slickNext:true,
      slickPrev:true,
    });
  
  });

});