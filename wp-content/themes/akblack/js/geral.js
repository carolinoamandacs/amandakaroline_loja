$(function(){


	/*****************************************
        SCRIPTS SPOT DO PRODUTO
    *******************************************/
    $( "section.spotsProdutos ul li" ).mouseenter(function(e){
       let imgFoco = $(this).children().children().children("img").attr("data-imgFoco");
       $(this).children().children().children("img").attr("src",imgFoco);
    }).mouseleave(function(e){
        let imgPadrao = $(this).children().children().children("img").attr("data-imgPadrao");
        $(this).children().children().children("img").attr("src",imgPadrao);
    });


    $( "section.hovercarroseis .item .spot" ).mouseenter(function(e){
       let imgFoco = $(this).children().children().children("img").attr("data-imgFoco");
       $(this).children().children().children("img").attr("src",imgFoco);
    }).mouseleave(function(e){
        let imgPadrao = $(this).children().children().children("img").attr("data-imgPadrao");
        $(this).children().children().children("img").attr("src",imgPadrao);
    });


    $(".pgPrivacidade .secao-ajuda ul li .pergunta").click(function(e) {

        $(".pgPrivacidade .secao-ajuda ul li .pergunta").removeClass("ativo");

        $(this).addClass("ativo");

        $(".pgPrivacidade .secao-ajuda ul li .resposta").addClass("ativo");

        $(this).next().removeClass("ativo");

        $(".pgPrivacidade .secao-ajuda ul li").removeClass("ativo");

        $(this).parent().addClass("ativo");
    });


    /**********************************************************************
        SCRIPTS DO CARROSSEL DE PARCERIA
    **********************************************************************/
	$(window).bind('scroll', function () {
     
     var alturaScroll = $(window).scrollTop();
     
       if (alturaScroll >= 200) {

          $(".topo").addClass("topoativo");

       } else {

            $(".topo").removeClass("topoativo");

       }
   });
    $(".pg-produto .produto .galeria figure img").click(function(e){
        let url_imagem = $(this).attr("src");
        console.log(url_imagem);
        $('.pg-produto .produto .imgImagemDestacada img').attr("src",url_imagem);
    });

    $(".pg-produto .produto .galeriaResponsiva .carrosselGaleriaProdutos .item img").click(function(e){
        let url_imagem = $(this).attr("src");
        console.log(url_imagem);
        $('.pg-produto .produto .imgImagemDestacada img').attr("src",url_imagem);
    });
    $("#ex1").zoom();


    $('.pg-produto .produto .descircaoProduto .breveDescricaoProduto a').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
        
    });

    $('#possoEnviar').click(function(e){
        e.preventDefault();
        $('.pgPrivacidade .pg-posso-enviar').fadeIn();
        $('body').addClass('travarScroll');
    });  

      $('#enviarAgora').click(function(e){
        e.preventDefault();
        $('.pgPrivacidade .pecas-em-casa').fadeIn();
        $('body').addClass('travarScroll');
    });

    $(' #possoEnviar a').click(function(e){
        e.preventDefault();
    });

    $('.pgPrivacidade .pg-posso-enviar .secao-posso-enviar .botao-fechar button').click(function(){
        $('body').removeClass('travarScroll');
        $('.pgPrivacidade .pg-posso-enviar').fadeOut();
    });

    $('#agendarCasa a').click(function(e){
        e.preventDefault();
        $(this).attr('data-titulo-form');  
        $( "#tituloFormulario" ).text($(this).attr('data-titulo-form')); 
  
        $('.pgPrivacidade .pecas-em-casa').fadeIn();
        $('body').addClass('travarScroll');

    });

    $('#agendarCaixa a').click(function(e){
        e.preventDefault();
        $(this).attr('data-titulo-form');     
        $('.pgPrivacidade .pecas-em-casa').fadeIn();
        $('body').addClass('travarScroll');
        $( "#tituloFormulario" ).text( $(this).attr('data-titulo-form')); 
    });

    $('#solicitar').click(function(){
        $('#enviarSolicitacao').trigger('click');

    });

    $('.pgPrivacidade .pecas-em-casa .secao-pecas-em-casa .fechar-solicitar .botao button').click(function(){
        $('body').removeClass('travarScroll');
        $('.pgPrivacidade .pecas-em-casa').fadeOut();
    });







    $(document).ready(function() {
            $('.reload').click(function() {
                location.reload();
            });
            $('.btnComprarClique').click(function(e) {
                //e.preventDefault();
                 $(".loaderSucessoCarrinho").addClass("openAviso");
                var productId = $(this).attr('data-productId');
                addToCart(productId);
            return false;
            });
            function addToCart(product_id) {
                
                jQuery.ajax({
                    type: 'POST',
                    url: '/projetos/amandakaroline_loja/?post_type=product&add-to-cart='+product_id,
                    data: { 'product_id':  product_id,
                    'quantity': '1'},


                    success: function(response, textStatus, jqXHR){

                       
                       

                    },
                    // dataType: 'JSON'
                });
            }

        });



    

});