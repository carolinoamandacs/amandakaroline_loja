<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	//echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
global $product;
global $configuracao;
//PREÇOS
$precoFormatado = $product->get_price();
$precoFormatado = str_replace('.', ',', $precoFormatado);
$preco = $precoFormatado;

$fotoProduto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$fotoProduto = $fotoProduto[0];

?>

<div class="pg-produto">
	<div class="container">
		<nav>
			<a href="<?php echo get_home_url(); ?>">AKBlack <i class="fas fa-angle-right"></i></a>
			<a href="<?php echo get_category_link( $product->category_ids[0] ) ?>"><?php echo get_the_category_by_ID($product->category_ids[0]) ?> <i class="fas fa-angle-right"></i></a>
			<?php if($product->category_ids[1]): ?>
				<a href="<?php echo get_category_link( $product->category_ids[1] ) ?>"><?php echo get_the_category_by_ID($product->category_ids[1]) ?> <i class="fas fa-angle-right"></i></a>
			<?php endif; ?>
			<a href="" class="selecionado"><?php echo get_the_title(); ?> <i class="fas fa-angle-right"></i></a>
		</nav>
	
		<div class="produto">
			<div class="row">

				<div class="col-md-2">
					<div class="galeria">
						<figure>
							<img src="<?php echo $fotoProduto  ?>" alt="<?php echo get_the_title(); ?>">
						</figure>
						<?php
							global $product;
							$minitaturas = $product->get_gallery_attachment_ids();

							foreach( $minitaturas as $miniatura): 

								$miniatura_url = wp_get_attachment_url( $miniatura );

						?>
						<figure>
							<img src="<?php echo $miniatura_url ?>" alt="<?php echo get_the_title(); ?>">
						</figure>
						<?php endforeach; ?>	
					</div>
				</div>

				<div class="col-md-6 col-sm-6">
					<!-- FOTO PTODUTO -->
					
					<figure class="zoom imgImagemDestacada"  id='ex1'>
						<img src="<?php echo $fotoProduto ?>" alt="<?php echo get_the_title(); ?>">
					</figure>
					<div class="galeriaResponsiva" style="display: none;">
						<div id="carrosselGaleriaProdutos" class="carrosselGaleriaProdutos">
							<?php
								global $product;
								$minitaturas = $product->get_gallery_attachment_ids();
								foreach( $minitaturas as $miniatura): 
									$miniatura_url = wp_get_attachment_url( $miniatura );
							?>
							<div class="item">
								<img src="<?php echo $miniatura_url ?>" alt="<?php echo get_the_title(); ?>">
							</div>
							<?php endforeach; ?>	
						</div>
					</div>
				</div>

				<div class="col-md-4 col-sm-6">
					<article class="descircaoProduto">
						<div class="nomeProduto">
							<h1><?php echo get_the_title(); ?></h1>
							<span>Ref: <?php echo $product->get_sku(); ?></span>
						</div>	
						<div class="breveDescricaoProduto">
							<p><?php echo get_the_excerpt();?></p>
						    <a href="#detalhesProduto" class="verMaisDescricaoProduto">Ver mais*</a> 			
						</div>
						<div class="precoProduto">
							<div class="row">
								<div class="col-sm-6">
									<?php 
									if ($product->get_price_html()):
										echo $product->get_price_html();
										
										else: ?>

											<strong>R$<?php echo $preco ?></strong>
											
										<?php endif; ?>

										<!-- <span>3x de R$15,00 sem juros</span> -->
										<?php do_action('woocommerce_single_product_summary'); ?>
									</div>

									<div class="col-sm-6">
									<!-- LINKS DE COMPARTILHAMENTO -->
									<div class="compartilharProduto">
										<ul class="listaRedesSociais">
										 	<li><a  href="https://www.facebook.com/sharer.php?u=<?php the_permalink() ?>" target="_blank" title="Compartilhar <?php the_title();?> no Facebook" class="facebook"><i class="fab fa-facebook-square"></i></a></li>
										 	<li><a href="https://web.whatsapp.com/send?text=<?php the_permalink() ?>" target="_blank" class="pinterest"><i class="fab fa-whatsapp"></i></a></li>
										 	<li><a href="http://twitter.com/intent/tweet?text=<?php the_title();?>&url=<?php the_permalink();?>&via=seunomenotwitter" title="Twittar sobre <?php the_title();?>" target="_blank"class="twitter"><i class="fab fa-twitter"></i></a></li>
										</ul>
									</div>
									</div>
								</div>
							</div>
						
						<div class="descricaoCores hidden">
							<div class="row">
								<div class="col-xs-6">
									<div class="coresProduto">
										<span>Cor: Creme</span>
										<ul>
											<li><span style="background-color: #ffff00"><h6 class="hidden">Amarelo</h6></span>	</li>
											<li><span style="background-color: #000000"><h6 class="hidden">Preto</h6></span>	</li>
											<li><span style="background-color: #39d3a3"><h6 class="hidden">Verde</h6></span>	</li>
											<li><span style="background-color: #ff0400"><h6 class="hidden">Vermelho</h6></span>	</li>
										</ul>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="tamanhoProduto">
										<span>Tamanho</span>
										<ul>
											<li><span>P</span></li>
											<li><span>M</span></li>
											<li><span>G</span></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						
					</article>
					

					<!-- VERIFICAÇÃO STOCK -->
					<?php 
							$numleft  = $product->get_stock_quantity(); 
							if ($numleft != 0):
					 ?>
					<div class="botaoComprar hidden" >
						<?php
							/**
							 * woocommerce_after_shop_loop_item hook
							 *
							 * @hooked woocommerce_template_loop_add_to_cart - 10
							 */
							//do_action( 'woocommerce_after_shop_loop_item' );
						
						?>		
					</div>
					<?php else: ?>
					<div class="foraEstoque">
						<span>Fora do estoque</span>
					</div>
					<?php  endif; ?>

					

				</div>
			</div>
		</div>
		<div class="divisor"></div>
		
		<!-- ÁREA DE DETLAHES DO PRODUTO  -->
		<section class="detalhesProduto" id="detalhesProduto">
			<h6>Detalhes</h6>
			<?php echo the_content();?>
		</section>
		
		

		<!-- BANNER DE INFORMAÇÕES -->
		<?php if ($configuracao['opt_bannerVanteagens']['url']): ?>
		<div class="informacoesLoja">
			<figure>
				<!-- <img src="<?php// echo $configuracao['opt_bannerVanteagens']['url'] ?>" alt="Informações"> -->
			</figure>
		</div>
	<?php endif; ?>
	</div>
	<!-- PRODUTOS RELACIONADOS -->
		<?php echo get_template_part( 'views/parts/produtosRelacionadoCarrossel', 'produtosRelacionadoCarrossel' ); ?>
</div>



