<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<?php 
$desconto = rwmb_meta('Akablack_desconto'); 
$produtoNovo = rwmb_meta('Akablack_produto_novo'); 
if(($desconto != '' && $desconto != null) && ($produtoNovo == 1)):
?>
<li class="spot desconto novo" data-desconto="<?php echo $desconto; ?>">
<?php elseif($desconto != '' && $desconto != null): ?>
<li class="spot desconto" data-desconto="<?php echo $desconto; ?>">
<?php elseif($produtoNovo == 1): ?>
<li class="spot novo">
<?php else: ?>
<li class="spot">
<?php 
	endif; 
		$imagemPostDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
		$imagemPostDestaque = $imagemPostDestaque[0];
		$product_miniatura = rwmb_meta('Akablack_product_miniatura');

		foreach ($product_miniatura as $product_miniatura) {
			$product_miniatura = $product_miniatura['full_url'];
		}
	?>
	<a href="<?php echo get_permalink(); ?>">
		<figure>
			<img src="<?php echo $imagemPostDestaque ?>" data-imgFoco="<?php echo $product_miniatura ?>" data-imgPadrao="<?php echo $imagemPostDestaque ?>" alt="">
		</figure>
	</a>
	<h2><?php echo get_the_title(); ?></h2>
	<div class="precos">
		<?php 
			$precoOriginal = $product->price;
			$regular_price = $product->regular_price;
			$precoQuebrado = explode('.', $precoOriginal);
			$precoFormatado = $precoQuebrado[0] .','. $precoQuebrado[1];
		
		?>
		<!-- <span class="parcelamento">6x de R$10,00</span> -->
		<!-- <strong>R$60,00</strong> -->
		<?php if ($precoOriginal < $regular_price): ?>
		<strong class="promocao"><b>De <s>R$<?php echo $regular_price ?></s></b> Por R$<?php echo  $product->price; ?></strong>
		<?php else: ?>
		<strong>R$<?php echo  $product->price; ?></strong>
		<?php endif; ?>
	</div>
	<div class="btnComprar">
		<!-- <input value="Comprar" name="" type="submit"> -->
		<?php
		/**
		 * woocommerce_after_shop_loop_item hook
		 *
		 * @hooked woocommerce_template_loop_add_to_cart - 10
		 */
		do_action( 'woocommerce_after_shop_loop_item' );

		?>
	</div>			
	
	<?php
	/**
	 * Hook: woocommerce_before_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	//do_action( 'woocommerce_before_shop_loop_item' );

	/**
	 * Hook: woocommerce_before_shop_loop_item_title.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	//do_action( 'woocommerce_before_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	//do_action( 'woocommerce_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	//do_action( 'woocommerce_after_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	//do_action( 'woocommerce_after_shop_loop_item' );
	?>
</li>

