<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AKBLACK
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</aside><!-- #secondary -->


<script>
	$(document).ready(function(){
		let textoFiltro = $('.woocommerce .widget_price_filter .price_slider_amount .button').text();
		let reText = textoFiltro.replace('Filtro', 'Filtrar');
		$('.woocommerce .widget_price_filter .price_slider_amount .button').text(reText);
	});
</script>