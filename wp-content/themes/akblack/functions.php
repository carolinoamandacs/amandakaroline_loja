<?php
/**
 * AKBLACK functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package AKBLACK
 */

if ( ! function_exists( 'akblack_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function akblack_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on AKBLACK, use a find and replace
		 * to change 'akblack' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'akblack', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'akblack' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'akblack_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'akblack_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function akblack_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'akblack_content_width', 640 );
}
add_action( 'after_setup_theme', 'akblack_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function akblack_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'akblack' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'akblack' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'akblack_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function akblack_scripts() {
	

 	//FONTS
    wp_enqueue_style( 'google-font', 'https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700|Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Roboto+Condensed:300,300i,400,400i,700,700i|Roboto+Slab:100,300,400,700');

  $iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
	$ipad = strpos($_SERVER['HTTP_USER_AGENT'],"iPad");
	$android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
	$palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
	$berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
	$ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");
	$symbian =  strpos($_SERVER['HTTP_USER_AGENT'],"Symbian");

	if ($iphone || $ipad || $android || $palmpre || $ipod || $berry || $symbian == true) {
		 wp_enqueue_style( 'akblack-slick', get_template_directory_uri() . '/css/slick.css');
	    wp_enqueue_style( 'akblack-slick-theme', get_template_directory_uri() . '/css/slick-theme.css');
		wp_enqueue_style( 'akblack-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
		wp_enqueue_style( 'akblack-mobile', get_template_directory_uri() . '/css/mobile.css');


		wp_enqueue_script( 'akblack-jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js' );
		wp_enqueue_script( 'akblack-slick', get_template_directory_uri() . '/js/slick.min.js' );
		wp_enqueue_script( 'akblack-bootsrap', get_template_directory_uri() . '/js/bootstrap.min.js' );
		wp_enqueue_script( 'js_carrossel_mobile-js_carrossel', get_template_directory_uri() . '/js/js_carrossel_mobile.js' );
		wp_enqueue_script( 'akblack-mobile', get_template_directory_uri() . '/js/mobile.js' );
	} else {
	    //JAVA SCRIPT
	    wp_enqueue_script( 'akblack-jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js' );
	    wp_enqueue_script( 'akblack-slick', get_template_directory_uri() . '/js/slick.min.js' );
	    wp_enqueue_script( 'akblack-js_carrossel', get_template_directory_uri() . '/js/js_carrossel.js' );
	    wp_enqueue_script( 'akblack-bootsrap', get_template_directory_uri() . '/js/bootstrap.min.js' );
	    wp_enqueue_script( 'akblack-zoom', get_template_directory_uri() . '/js/jquery.zoom.min.js' );
	    wp_enqueue_script( 'akblack-geral', get_template_directory_uri() . '/js/geral.js' );
  

	    //CSS
	    wp_enqueue_style( 'akblack-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
	    wp_enqueue_style( 'akblack-bootstrap-font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css');
	    wp_enqueue_style( 'akblack-slick', get_template_directory_uri() . '/css/slick.css');
	    wp_enqueue_style( 'akblack-slick-theme', get_template_directory_uri() . '/css/slick-theme.css');
	    wp_enqueue_style( 'akblack-animate', get_template_directory_uri() . '/css/animate.css');
	    wp_enqueue_style( 'akblack-mobile', get_template_directory_uri() . '/css/mobile.css');
	    wp_enqueue_style( 'akblack-style', get_stylesheet_uri() );
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'akblack_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
//  */
// if ( class_exists( 'WooCommerce' ) ) {
// 	require get_template_directory() . '/inc/woocommerce.php';
// }

/**
 *  Configurações via redux
 */
if (class_exists('ReduxFramework')) {
    require_once (get_template_directory() . '/redux/sample-config.php');
}

//FUNÇÃO PARA REGISTRAR MENU
function register_my_menus() {
	register_nav_menus(
		array(
			'topo' => __('Menu Principal Loja'),
		)
	);
}
add_action( 'init', 'register_my_menus' );

// CUSTOM EXCERPT
function customExcerpt($qtdCaracteres) {
  $excerpt = get_the_excerpt();
  $qtdCaracteres++;
  if ( mb_strlen( $excerpt ) > $qtdCaracteres ) {
    $subex = mb_substr( $excerpt, 0, $qtdCaracteres - 5 );
    $exwords = explode( ' ', $subex );
    $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
    if ( $excut < 0 ) {
      echo trim(mb_substr( $subex, 0, $excut ));
    } else {
      echo trim($subex);
    }
    echo trim('...');
  } else {
    echo trim($excerpt);
  }
}




// VERSIONAMENTO DE FOLHAS DE ESTILO
function my_wp_default_styles($styles){
	$styles->default_version = "15042019";
}

add_action("wp_default_styles", "my_wp_default_styles");

	// VERSIONAMENTO DE FOLHAS DE ESTILO
function my_wp_default_scripts($scripts){
	$scripts->default_version = "15042019";
}
add_action("wp_default_scripts", "my_wp_default_scripts");




function get_carrosselDestaque($atts) {
	
	return get_template_part( 'views/parts/carrosselDestaque', 'carrosselDestaque' );
}
add_shortcode('carrosselDestaque', 'get_carrosselDestaque'); 

function get_menuCategoria($atts) {
	
	return get_template_part( 'views/parts/menuCategoria', 'menuCategoria' );
}
add_shortcode('menuCategoria', 'get_menuCategoria'); 

function get_mosaico($atts) {
	
	return get_template_part( 'views/parts/mosaico', 'mosaico' );
}
add_shortcode('mosaico', 'get_mosaico'); 

function get_bannerDestaque($atts) {
	
	return get_template_part( 'views/parts/bannerDestaque', 'bannerDestaque' );
}
add_shortcode('bannerDestaque', 'get_bannerDestaque'); 


function get_parceiros($atts) {
	
	return get_template_part( 'views/parts/parceiros', 'parceiros' );
}
add_shortcode('parceiros', 'get_parceiros'); 


function get_bannerVanteagens($atts) {
	
	return get_template_part( 'views/parts/bannerVanteagens', 'bannerVanteagens' );
}
add_shortcode('bannerVanteagens', 'get_bannerVanteagens'); 

function get_spotCarrossel($atts) {
	
	return get_template_part( 'views/parts/spotCarrossel', 'spotCarrossel' );
}
add_shortcode('spotCarrossel', 'get_spotCarrossel'); 

function get_produtosRelacionadoCarrossel($atts) {
	
	return get_template_part( 'views/parts/produtosRelacionadoCarrossel', 'produtosRelacionadoCarrossel' );
}
add_shortcode('produtosRelacionadoCarrossel', 'get_produtosRelacionadoCarrossel'); 

function get_postsBlog($atts) {
	
	return get_template_part( 'views/parts/postsBlog', 'postsBlog' );
}
add_shortcode('postsBlog', 'get_postsBlog'); 

function get_sideBar_categoria($atts) {
	
	return get_template_part( 'views/parts/sideBar_categoria', 'sideBar_categoria' );
}
add_shortcode('sideBar_categoria', 'get_sideBar_categoria'); 

function get_sideBar_suporte($atts) {
	
	return get_template_part( 'views/parts/sideBar_suporte', 'sideBar_suporte' );
}
add_shortcode('sideBar_suporte', 'get_sideBar_suporte'); 

function get_instagram($atts) {
	
	return get_template_part( 'views/parts/instagram', 'instagram' );
}
add_shortcode('instagram', 'get_instagram'); 

function custom_upload_mimes( $existing_mimes = array() ) {
    $existing_mimes['rar'] = 'application/x-rar-compressed';
    return $existing_mimes;
}
add_filter( 'upload_mimes', 'custom_upload_mimes' );


function get_carrosselProduto($argumento){
	
	// $categoria = shortcode_atts( array (
	//   'catgoria' => 'Invitado',
	 
	//   ), $argumento );
		
		
	return get_template_part( 'views/parts/template_carrossel_produto');

	
}
add_shortcode('carrosselProduto','get_carrosselProduto');

function get_header_mobile($argumento){
	return get_template_part( 'views/parts/header_mobile');
}
add_shortcode('header_mobile','get_header_mobile');

function get_header_desktop($argumento){
	return get_template_part( 'views/parts/header_desktop');
}
add_shortcode('header_desktop','get_header_desktop');


function get_footer_mobile($argumento){
	return get_template_part( 'views/parts/footer_mobile');
}
add_shortcode('footer_mobile','get_footer_mobile');

function get_footer_desktop($argumento){
	return get_template_part( 'views/parts/footer_desktop');
}
add_shortcode('footer_desktop','get_footer_desktop');


function get_layout_mobile($argumento){
	return get_template_part( 'views/parts/layout_mobile');
}
add_shortcode('layout_mobile','get_layout_mobile');