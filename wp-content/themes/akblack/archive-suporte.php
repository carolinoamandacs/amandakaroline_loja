<?php 
/**
* The template for displaying archive pages
*
*   @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*	@package AKBLACK
*/
get_header();

?>
<div class="pgPrivacidade">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="menu-caminho">
					<nav>
						<a href="#">AKBlack <i class="fas fa-angle-right"></i></a>
						<a href="#" class="selecionado">Autoatendimento<i class="fas fa-angle-right"></i></a>
					</nav>
				</div>
			</div>

			<div class="col-md-9">
				<h1>Precisa de um <strong>Help</strong>?</h1>
			</div>
		</div>
		
		<div class="row">
			<?php echo do_shortcode('[sideBar_suporte]'); ?>

			<!-- ÁREA TEXTO CADASTRO(AJUDA E SUPORTE) -->
			<div class="col-md-9">

				<!-- SEÇÃO AJUDA E SUPORTE -->
				<section class="secao-ajuda">
					<h6 class="hidden">SEÇÃO AJUDA E SUPORTE</h6>
					<ul>
						<?php 
							$ajudaesuporte = new WP_Query( 
								array(
									'post_type' => 'suporte', 
									'posts_per_page' => -1,
								)
							);
							while($ajudaesuporte->have_posts()):
								$ajudaesuporte->the_post();
						?>
						<li>
							<h2 class="pergunta"><?php echo get_the_title(); ?></h2>
							<div class="resposta ativo">
								<?php echo the_content(); ?>
							</div>
						</li>
					<?php endwhile; wp_reset_query(); ?>


					</ul>
				</section>
				

				
				<!-- <section class="contato">
					<p>Entre em contato:</p>
					<?php echo do_shortcode('[contact-form-7 id="294" title="Contato"]'); ?>
				</section> -->
			</div>

		</div>
						<!-- SEÇÃO CONTATO PG PRIVACIDADE -->
				<section class="secao-contato">
					<h6 class="hidden">CONTATO PÁGINA PRIVACIDADE</h6>

					<div class="duvida">
						<h2>AINDA TA COM DÚVIDA?</h2>
						<p>Abre o coração pra gente.</p>
					</div>
					
					<div class="row">
						<div class="col-sm-4">
							<div class="contato chat">
								<a href="#">						
									<figure>
										<img src="<?php echo get_template_directory_uri(); ?>/img/chat.png" alt="Chat">
									</figure>
									<p>CHAT</p>
									<span>Eaí, quer TC?</span>
								</a>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="contato email">
								<a href="mailto:<?php echo $configuracao['opt_email'] ?>">
									<figure>
										<img src="<?php echo get_template_directory_uri(); ?>/img/envelope.png" alt="E-mail">
									</figure>
									<p>E-MAIL</p>
									<span>É só mandar :)</span>
								</a>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="contato fone">
								<a href="tel:<?php echo $configuracao['opt_telefone'] ?>">
									<figure>
										<img src="<?php echo get_template_directory_uri(); ?>/img/telefone.png" alt="Telefone">
									</figure>
									<p>TELEFONE</p>
									<span><?php echo $configuracao['opt_telefone'] ?></span>
								</a>
							</div>
						</div>
					</div>
				</section>
	</div>

</div>
<?php get_footer(); ?>