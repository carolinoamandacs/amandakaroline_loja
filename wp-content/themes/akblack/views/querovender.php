<?php 
/**
*	Template Name: Quero Vender
*	Template Description: Template para a página Quero Vender
*	
*	@package AKBLACK
*/
get_header();
$fotoDestaque  = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$fotoDestaque = $fotoDestaque[0];
?>
<div class="pgPrivacidade">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="menu-caminho">
					<nav>
						<a href="#">AKBlack <i class="fas fa-angle-right"></i></a>
						<a href="#" class="selecionado">Autoatendimento<i class="fas fa-angle-right"></i></a>
					</nav>
				</div>
			</div>

			<div class="col-md-9">
				<!-- <h1>Precisa de um <strong>Help</strong>?</h1> -->
			</div>
		</div>
		<div class="row">
			<?php echo the_content(); ?>

			<!-- ÁREA TEXTO QUERO VENDER(PARCERIAS) -->
			<div class="col-md-9">

				<!-- SEÇÃO QUERO VENDER -->
				<section class="secao-quero-vender">
					<h6 class="hidden">QUERO VENDER</h6>

					<h3>Veja como é simples e fácil vender com AKBlack!</h3>

					<div class="banner-principal-passos">
						<figure>
							<img src="<?php echo $fotoDestaque ?>" alt="<?php get_the_content() ?>">
						</figure>
					</div>

					<div class="saiba-como">
						<div class="row">
							<div class="col-sm-3">
								<div class="" id="possoEnviar">
								<a href="#">
									<figure class="etiqueta">
										<img src="<?php echo get_template_directory_uri(); ?>/img/casual-t-shirt-.png" alt="Etiqueta">
									</figure>
									<p>O QUE POSSO ENVIAR?</p>
								</a>
								</div>
							</div>

							<div class="col-sm-3">
								<a href="#">
									<figure class="caminhao">
										<img src="<?php echo get_template_directory_uri(); ?>/img/caminhao.png" alt="Caminhão">
									</figure>
									<p>COMO ENVIAR?</p>
								</a>
							</div>

							<div class="col-sm-3">
								<a href="#">
									<figure class="dinheiro">
										<img src="<?php echo get_template_directory_uri(); ?>/img/dinheiro.png" alt="Dinheiro">
									</figure>
									<p>QUANDO RECEBO?</p>
								</a>
							</div>

							<div class="col-sm-3">
								<a href="#">
									<figure class="duvida">
										<img src="<?php echo get_template_directory_uri(); ?>/img/duvida.png" alt="Dúvida">
									</figure>
									<p>DÚVIDAS</p>
								</a>
							</div>
						</div>
					</div>

					<div class="envie-agora">
						<div class="botao-envie-agora" >
							<button type="submit" id="enviarAgora">ENVIE AGORA!</button>
						</div>
						<div class="row">

							<div class="col-sm-6">
								<div class="agendar agendar-casa " id="agendarCasa" >
									<a href="#" data-titulo-form="AGENDAR RETIRADA EM CASA">
										<figure class="casa">
											<img src="<?php echo get_template_directory_uri(); ?>/img/casa.png" alt="Casa">
										</figure>
										<p id="tituloFormularioCasa">AGENDAR RETIRADA EM CASA</p>
									</a>
									<span>*Apenas Guarapuava</span>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="agendar agendar-caixa"  id="agendarCaixa">
									<a href="#" data-titulo-form="ENVIAR PELOS CORREIOS">
										<figure class="caixa">
											<img src="<?php echo get_template_directory_uri(); ?>/img/caixa.png" alt="Caixa">
										</figure>
										<p id="tituloFormularioCorreio">ENVIAR PELOS CORREIOS</p>
									</a>
									<span>*Frete Grátis</span>
								</div>
							</div>

						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
	
	<!-- PG POP-UP O QUE POSSO ENVIAR -->
	<div class="pg-posso-enviar" style="display: none">
		<div class="container">

			<section class="secao-posso-enviar">
				<h6 class="hidden">SEÇÃO O QUE POSSO ENVIAR</h6>
				
				<h2>O QUE POSSO ENVIAR?</h2>

				<div class="area-pecas-aceitas">
					<div class="row">
						<div class="pecas-aceitas">
							<div class="col-sm-6">

								<div class="pecas aceitamos">
									<div class="imagem aceitamos">
										<figure>
											<img src="<?php echo get_template_directory_uri(); ?>/img/aceitamos.png" alt="">
										</figure>
										<p>ACEITAMOS</p>
									</div>
									<ul>
										<li>Roupas e acessórios femininos(bolsas, sapatos, óculos, relógios, cintos, carteiras, colares, lenços);</li>
										<li>Peças originais novas, semi-novas e usadas em perfeitos estado, limpas e lavadas;</li>
										<li>Biquínis e peças fitness somente novas e com etiqueta e nunca usados.</li>
									</ul>
								</div>

							</div>
							<div class="col-sm-6">

								<div class="pecas nao-aceitamos">
									<div class="imagem nao-aceitamos">
										<figure>
											<img src="<?php echo get_template_directory_uri(); ?>/img/nao-aceitamos.png" alt="">
										</figure>
										<p>NÃO ACEITAMOS</p>
									</div>
									<ul>
										<li>Itens com defeitos, manchas, marcas de uso ou peças faltantes;</li>
										<li>Peças íntimas e pijamas;</li>
										<li>Peças sem etiqueta da marca/tamanho;</li>
										<li>Biquínis usados/sem etiqueta.</li>
									</ul>
								</div>

							</div>
						</div>
					</div>
				</div>
				<div class="botao-fechar">
					<button type="button">FECHAR</button>
				</div>
			</section>
		</div>
	</div>

	<!-- PG POP-UP PEÇAS EM CASA FORMULÁRIO -->
	<div class="pecas-em-casa" style="display: none">
		<div class="container">

			<!-- SEÇÃO RETIRAR PEÇAS EM CASA -->
			<section class="secao-pecas-em-casa">
				<h6 class="hidden">SEÇÃO RETIRAR PEÇAS EM CASA</h6>

				<h2 id="tituloFormulario">AGENDAMENTO DE RETIRADA DE PEÇAS EM CASA</h2>

				<div class="area-formulario">
					<div class="row">
						<div class="formulario">
							<div class="col-sm-3">
								<form action="" class="form-menor">
									<label>Quantidade de peças</label>
									<input class="input-menor" type="text" placeholder="Min 15">
								</form>
							</div>
							<div class="col-sm-9">
								<div class="row">
                                    <?php echo do_shortcode('[contact-form-7 id="602" title="Contato"]'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="fechar-solicitar">
					<div class="row">

						<div class="col-sm-6">
							<div class="botao fechar">
								<button type="button">FECHAR</button>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="botao solicitar" id="solicitar">
								<input type="submit"  value="SOLICITAR"></input>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
<?php get_footer(); ?>