<?php 
/**
*	Template Name: Quem Somos
*	Template Description: Template para a página Quem Somos
*
*	@package AKBLACK
*/ 
get_header(); 
?>
<!-- PÁGINA QUEM SOMOS -->
<div class="pg-quem-somos">
	<div class="container">
		<nav>
			<a href="<?php echo get_home_url(); ?>" class="selecionado">AKBlack<i class="fas fa-angle-right"></i></a>
			<a href="#" class="selecionado">Quem Somos<i class="fas fa-angle-right"></i></a>
		</nav>
		<figure class="banner-principal">
			<img src="<?php echo $configuracao['quem_somos_banner']['url'] ?>" alt="Quem Somos">
		</figure>
		<article class="primeiro-texto">
			<?php echo $configuracao['quem_somos_descricao']; ?>
		</article>
		<div class="atributos">
			<div class="row">
				<div class="modelo-atributos">
					<div class="col-sm-5">
						<figure>
							<img src="<?php echo $configuracao['quem_somos_vantagens_imagem']['url']; ?>" alt="Vantagens">
						</figure>
					</div>
					<div class="col-sm-7">
						<ul>
							<?php 
								$textos = $configuracao['quem_somos_vantagens_textos']; 
								foreach ($textos as $textos) :
							?>
								<li><?php echo $textos; ?></li>
							<?php endforeach; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
		<div class="frase-destaque">
			<div class="row">
				<div class="frase-modelos correctionSkew">
					<div class="col-sm-7">
						<div class="frase">
							<?php 
								$textosMotivacionais = $configuracao['quem_somos_motivacional_textos']; 
								for ($i=0; $i < sizeof($textosMotivacionais); $i++){ 
							?>
									<p class="<?php echo 'p'.($i+1); ?>"><?php echo $textosMotivacionais[$i] ?></p>
							<?php } ?>
						</div>
					</div>
					<div class="col-sm-5">
						<div class="imagens">
							<figure class="modelo modelo2">
								<img class="imagem img-modelo2" src="<?php echo $configuracao['quem_somos_motivacional_imagem_cima']['url']; ?>" alt="Quem somos">
							</figure>
							<figure class="modelo modelo3">
								<img class="imagem img-modelo3" src="<?php echo $configuracao['quem_somos_motivacional_imagem_baixo']['url']; ?>" alt="Quem somos">
							</figure>
						</div>
					</div>
				</div>
			</div>
		</div>
	<div class="container">
		<article class="segundo-texto">
			<?php echo $configuracao['quem_somos_final_conteudo']; ?>
		</article>
	</div>
</div>
<?php get_footer(); ?>