<?php 
/**
*	Template Name: Termos 
*	Template Description: Template Para Termos 
*
*	@package AKBLACK
*/
get_header();

	$fotoDestaque  = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	$fotoDestaque = $fotoDestaque[0];

?>
<!-- PÁGINA DE PRIVACIDADE -->
<div class="pgPrivacidade">
	<div class="container">

		<div class="row">
			<div class="col-md-3">
				<div class="menu-caminho">
					<nav>
						<a href="#">AKBlack <i class="fas fa-angle-right"></i></a>
						<a href="#" class="selecionado">Autoatendimento<i class="fas fa-angle-right"></i></a>
					</nav>
				</div>
			</div>

			<div class="col-md-9">
				
				<!-- <h1>Precisa de um <strong>Help</strong>?</h1> -->
			</div>
		</div>

		<div class="row">
			<!-- SIDEBAR MENU ÁREAS PRINCIPAIS -->
			<?php echo do_shortcode('[sideBar_suporte]'); ?>

			<!-- ÁREA TEXTO TERMOS E CONDIÇÕES(PRIVACIDADE) -->
			<div class="col-md-9">
				<?php if ($fotoDestaque):?>
				<section class="bannerDestaque pgCategoriaBanner">
					<h6 class="hidden"> <?php echo get_the_title() ?></h6>
					<a href="">
						<img src="<?php echo $fotoDestaque ?> " alt="<?php  echo get_the_title() ?>">
					</a>
				</section>
			<?php endif; ?>
				<!-- SEÇÃO DE TERMOS DE TERMOS E CONDIÇÕES -->
				<section class="secao-termos">
					<?php echo the_content(); ?>
				</section>
			</div>

		</div>

		<!-- SEÇÃO CONTATO PG PRIVACIDADE -->
		<section class="secao-contato">
			<h6 class="hidden">CONTATO PÁGINA PRIVACIDADE</h6>

			<div class="duvida">
				<h2>AINDA TA COM DÚVIDA?</h2>
				<p>Abre o coração pra gente.</p>
			</div>
			
			<div class="row">
				<div class="col-sm-4">
					<div class="contato chat">
						<a href="#">						
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/img/chat.png" alt="Chat">
							</figure>
							<p>CHAT</p>
							<span>Eaí, quer TC?</span>
						</a>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="contato email">
						<a href="mailto:<?php echo $configuracao['opt_email'] ?>">
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/img/envelope.png" alt="E-mail">
							</figure>
							<p>E-MAIL</p>
							<span>É só mandar :)</span>
						</a>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="contato fone">
						<a href="tel:<?php echo $configuracao['opt_telefone'] ?>">
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/img/telefone.png" alt="Telefone">
							</figure>
							<p>TELEFONE</p>
							<span><?php echo $configuracao['opt_telefone'] ?></span>
						</a>
					</div>
				</div>
			</div>
		</section>

	</div>
</div>

<?php get_footer(); ?>