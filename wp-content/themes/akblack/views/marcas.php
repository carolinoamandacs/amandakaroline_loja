<?php 
/**
*	Template Name: Página de Marcas
*	Template Description: Template para a página de marcas.
*
*	@package AKBLACK
*/
get_header();
?>
<!-- PÁGINA MARCAS -->
<div class="pg-marcas">
	<div class="menu-caminho">
		<nav>
			<a href="<?php echo get_home_url(); ?>">AKBlack <i class="fas fa-angle-right"></i></a>
			<a href="#" class="selecionado">Marcas<i class="fas fa-angle-right"></i></a>
		</nav>
	</div>
	<!-- SEÇÃO DE MARCAS -->
	<section class="secao-marcas">
		<!-- ÁREA RENNER -->
		<div class="renner">

			<div class="row">
				
				<div class="col-sm-6">
					<figure class="logo-renner">
						<img src="<?php echo get_template_directory_uri(); ?>/img/logo-renner.png" alt="Logo Renner">
					</figure>
				</div>
				<div class="col-sm-3">
					<figure class="modelo-renner">
						<img src="<?php echo get_template_directory_uri(); ?>/img/modelo-renner.png" alt="Modelo Renner">
					</figure>
				</div>
				<div class="col-sm-3">
					<figure class="roupas-renner">
						<img src="<?php echo get_template_directory_uri(); ?>/img/roupas-renner.png" alt="Ropas Renner">
					</figure>
					<button type="button" class="botao-confira-renner">CONFIRA AQUI</button>
				</div>
			</div>
		</div>

		<!-- ÁREA PERNABUCANAS -->
		<div class="pernabucanas">
			<div class="row">
				<div class="col-sm-3">
					<figure class="roupas-pernabucanas">
						<img src="<?php echo get_template_directory_uri(); ?>/img/roupas-pernabucanas.png" alt="Roupas Pernabucanas">
					</figure>
					<button class="botao-confira-pernabucanas">CONFIRA AQUI</button>
				</div>
				<div class="col-sm-3">
					<figure class="modelo-pernabucanas">
						<img src="<?php echo get_template_directory_uri(); ?>/img/modelo-pernabucanas.png" alt="Modelo Pernabucanas">
					</figure>
				</div>
				<div class="col-sm-6">
					<figure class="logo-pernabucanas">
						<img src="<?php echo get_template_directory_uri(); ?>/img/logo-pernabucanas.png" alt="Logo Pernabucanas">
					</figure>
				</div>
			</div>
		</div>

		<!-- ÁREA KANUI -->
		<div class="kanui">
			<div class="row">
				<div class="col-sm-6">
					<figure class="logo-kanui">
						<img src="<?php echo get_template_directory_uri(); ?>/img/logo-kanui.png" alt="Logo Kanui">
					</figure>
				</div>
				<div class="col-sm-3">
					<figure class="modelo-kanui">
						<img src="<?php echo get_template_directory_uri(); ?>/img/modelo-kanui.png" alt="Modelo Kanui">
					</figure>
				</div>
				<div class="col-sm-3">
					<figure class="roupas-kanui">
						<img src="<?php echo get_template_directory_uri(); ?>/img/roupas-kanui.png" alt="Roupas Kanui">
					</figure>
					<button class="botao-confira-kanui">CONFIRA AQUI</button>
				</div>
			</div>
		</div>

	</section>
</div>
<?php get_footer(); ?>