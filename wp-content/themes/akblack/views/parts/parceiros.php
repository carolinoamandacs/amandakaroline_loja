<!-- SESSÃO  PARCERIAS-->
<?php 

	//$parceiros = new WP_Query( array( 'post_type' => 'parceiro', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );

if ($parceiros): ?>
<div class="container">
	<section class="carrosselParceria parceria">
		<h6>Parceiros</h6>

		<div  class="carrosselParceiros">
	
			<!-- ITEM -->
			<?php
				
				while ( $parceiros->have_posts() ) : $parceiros->the_post();
					$fotoParceiro = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$fotoParceiro = $fotoParceiro[0];
					$parceiro_link = rwmb_meta('Akablack_parceiro_link');	
			?>
			<!-- ITEM -->
			<figure class="item">
				<a href="<?php echo $parceiro_link ?>" target="_blank">
					<img class="img-responsive" src="<?php echo $fotoParceiro ?>" alt="<?php echo get_the_title() ?>">
				</a>
			</figure>
			<?php endwhile; wp_reset_query(); ?>
			
		</div>

		<div class="tornarParceiro">
			<div class="abrirModalPaceiro">
				<a class="abrirFormularioParceria">TORNAR-SE PARCEIRO</a>
			</div>
		</div>	
	</section>
</div> 
<?php endif; ?>
