<?php global $configuracao; if ($configuracao['opt_bannerVanteagens']['url']) ?>
<section class="bannerBeneficios">
	<div class="container">	
		<h6 class="hidden">Beneficios</h6>

		<div class="todosBeneficios">
			<figure>
				<img src="<?php echo $configuracao['opt_bannerVanteagens']['url'] ?>" alt="Banner Vantagens">
			</figure>
		</div>
	</div>	
</section>
<?php endif; ?>