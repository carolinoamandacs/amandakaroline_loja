<section class="menuCategoria">
	<div class="menu">
		<nav class="carrosselCategoria">
			<?php
				// PASSAMDO TERMOS DE CATEGORIA PAI PARA A FOLHO
				$categorias = get_terms( 'product_cat' , array(
					'orderby'    => '',
					'hide_empty' => 0,
					'child_of'     => 0,
					'parent'       => 0,
					'orderby'      => 'name',
					'pad_counts'   => 0,
					'hierarchical' => 1,
					'title_li'     => '',
					'hide_empty'   => 0
				));
			
				// LOOP DE CATEGORIAS FILHAS 
				foreach ($categorias as $categoria):
					
						// NOME
						$categoryName = $categoria->name;
						$categorySlug = $categoria->slug;
						$thumbnail_id = get_woocommerce_term_meta( $categoria->term_id, 'thumbnail_id', true );
						$icone = wp_get_attachment_url( $thumbnail_id );
						$linkCategoria = get_category_link($categoria->term_id);
						if ($icone):
			?>
			<a href="<?php echo $linkCategoria  ?>" class="item">
				<figure>
					<img src="<?php echo $icone ?>" alt="<?php echo $categoryName ?>">
				</figure>
					<h2><?php echo $categoryName ?></h2>
			</a>
			<?php endif;endforeach; ?>
		</nav>
	</div>
</section>	