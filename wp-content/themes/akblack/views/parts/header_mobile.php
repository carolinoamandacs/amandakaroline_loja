<?php 
global $configuracao;
global $woocommerce;
global $product;
global $current_user;
$urlMinhaConta 	= get_permalink(get_option('woocommerce_myaccount_page_id'));
$urlCarrinho    = WC()->cart->get_cart_url();
$urlCheckout 	= WC()->cart->get_checkout_url();
$urlLogout 	= wp_logout_url( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) );
// WOCOMMERCE: QUANTIDADE DE ITENS NO CARRINHO
	$qtdItensCarrinho 		= WC()->cart->cart_contents_count;
	$qtdItensCarrinhoRotulo = ($qtdItensCarrinho == 0 || $qtdItensCarrinho > 1) ? $qtdItensCarrinho . ' ' : $qtdItensCarrinho . ' ';
 ?>

<div class="loaderSucessoCarrinho">
	<div class="areaConteudo">
		<div class="icone">
			<i class="fas fa-cart-plus"></i>
		</div>
		<?php if ($current_user->display_name):?>
		<p><i class="fas fa-bullhorn"></i> Olá <strong><?php echo $current_user->display_name ?></strong>  seu produto foi adicionado com sucesso no carrinho!</p>
		<?php else: ?>
		<p><i class="fas fa-bullhorn"></i> Olá seu produto foi adicionado com sucesso no carrinho!</p>
		<?php endif; ?>
		<a href='<?php echo get_home_url()."/checkout/" ?>'> Clique aqui para finalizar sua compra <i class="fas fa-check-circle"></i></a>
		<span class="reload">Continuar comprando <i class="fas fa-plus"></i></span>
	</div>
</div>

 <div class="loadSite">
		<div class="areaload">
			<img src="<?php echo $configuracao['opt_logo']['url'] ?>" alt="<?php echo get_bloginfo() ?>">
		</div>
		<span class="loadEfects">
			<div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
		</span>
	</div>


	<div class="menuMobile">
		<div class="row">
			<div class="col-xs-3">
				<div class="logoMobile">
					<figure>
						<a href="<?php echo home_url('/');?>">
							<img src="<?php echo $configuracao['opt_logo']['url'] ?>" alt="<?php echo get_bloginfo() ?>">
						</a>
					</figure>
				</div>
			</div>	
			<div class="col-xs-9">
				<div class="menuNavegacao">	
					<div class="iconeNavegacao">
						<ul>
							<li>
								<a href="<?php echo get_home_url();echo"/cart/"?>">
									<i class="fas fa-shopping-cart"></i>
								</a>
							</li>
							<li>
								<a href="<?php echo $urlMinhaConta ?>">
									<i class="fas fa-user"></i>
								</a>
							</li>
							<li>
								<i id="openPesquisa" class="fas fa-search"></i>
								<i id="closePesquisa" class="fas fa-search"></i>
							</li>
							<li id="menuOpen">

								<i class="fas fa-bars"></i>
							
							</li>
						</ul>	
					</div>
				</div>	
			</div>
		</div>
		<div class="formPesquisaMobile">
			<?php echo do_shortcode('[wcas-search-form]'); ?>
		</div>
	</div>


	<header class="mobile" id="menuHomeInicial">
		
		<nav>
			<ul>
				<li>
					<a href="">
						<small>
						<i class="fas fa-store"></i> Loja AkBlakc
						</small>
					</a>
				</li>
				<li>
					<a href="https://blog.amandakaroline.com.br/" target="_blank">
						<small>
						<i class="fab fa-blogger-b"></i> Blog
						</small>
					</a>
				</li>
				<li>
					<a href="http://amandakaroline.com.br" target="_blank">
						<small>
						<i class="fas fa-star"></i> Plataforma AK
						</small>
					</a>
				</li>
				<li>
					<a href="">
						<small>
						<i class="fas fa-envelope"></i> Contato
						</small>
					</a>
				</li>
				
				<li>
					<a href="malito:contato@akblack.com.br">
						<small>
							<i class="fas fa-paper-plane"></i>
						 E-mail
						</small>
					</a>
				</li>

				<li>
					<a href="tel:4284068155">
						<small>
						<i class="fas fa-mobile-alt"></i>Telefone
						</small>
					</a>
				</li>

			</ul>
		</nav>		
	</header>

	<?php 

	global $post;
$product_categories = get_categories( array(
    'taxonomy'     => 'product_cat',
    'orderby'      => 'name',
    'pad_counts'   => false,
    'hierarchical' => 1,
    'hide_empty'   => false
) );


?>
	
	<nav class="menuInstitucional">
		<i id="closeMenuInstitucional" class="far fa-times-circle"></i>
		<i id="menuHome" class="fas fa-home"></i>
		<p>Menu</p>
		
		<ul>
			<?php foreach ($product_categories as $product_category):?>
			<li>
				<a href="<?php echo get_category_link($product_category->cat_ID); ?>"><?php echo $product_category->name ?></a>
			</li>
			<?php endforeach; ?>
			
		</ul>
	</nav>

