<?php 
	global $product;
		$related = $product->get_related();
	    $relatedQtd = count($related);
	    $upsells = $product->get_upsells();
	    $upsellsQtd = count($upsells);
	    $meta_query = WC()->query->get_meta_query();
	    $args = array(
			'post_type'           => 'product',
			'ignore_sticky_posts' => 1,
			'no_found_rows'       => 1,
			'posts_per_page'      => $upsellsQtd,
			'orderby'             => 'rand',
			'post__in'            => $upsells,
			'post__not_in'        => array( $product->id ),
			'meta_query'          => $meta_query
	    );
    	$loop = new WP_Query( $args );                
if ($loop):

?>
<section class="produtosRelacionados hovercarroseis">

	<h6>Veja também</h6>

	<div class="carrosselProdutosRelacionados carrosselProdutosSlick1">


	<?php while($loop ->have_posts()): $loop ->the_post(); ?>

		<?php echo get_template_part( 'views/parts/spotCarrossel', 'spotCarrossel' ); ?>

	<?php endwhile; wp_reset_query();?>
	</div>
</section>
<?php endif; ?>