<?php 
	global $configuracao;
	global $woocommerce;
	global $product;
	global $current_user;
	$urlMinhaConta 	= get_permalink(get_option('woocommerce_myaccount_page_id'));
	$urlCarrinho    = WC()->cart->get_cart_url();
	$urlCheckout 	= WC()->cart->get_checkout_url();
	$urlLogout 	= wp_logout_url( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) );
	//GALERIA DE IMAGENS

 ?>
<footer class="rodape">
	<div class="infoRodape">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<div class="logo">
						<a href="#">
							<img src="http://akblack.hcdesenvolvimentos.com.br/wp-content/uploads/2018/11/logo-topo.png" alt="AKBLACK">
						</a>
					</div>
				</div>
				<div class="col-md-2">
					<div class="menu">
						<strong>Institucional</strong>
						<nav>
							<a href="http://akblack.hcdesenvolvimentos.com.br/quem-somos/">Quem somos</a>
							<a href="http://akblack.hcdesenvolvimentos.com.br/termos-e-condicoes/">Termos e condições</a>
							<a href="http://akblack.hcdesenvolvimentos.com.br/politica-de-privacidade/">Politica de Privacidade</a>
							<a href="http://akblack.hcdesenvolvimentos.com.br/quero-vender/">Quero Vender!</a>
						</nav>
					</div>
				</div>
				<div class="col-md-2">
					<div class="menu">
						<strong>Minha conta</strong>
						<nav>
							<a href="http://akblack.hcdesenvolvimentos.com.br/minha-conta/">Meus dados</a>
							<a href="http://akblack.hcdesenvolvimentos.com.br/my-account/orders/">Meus pedidos</a>
							<a href="http://akblack.hcdesenvolvimentos.com.br/minha-conta/">Cadastrar</a>
						</nav>
					</div>
				</div>
				<div class="col-md-2">
					<div class="menu">
						<strong>Atendimento</strong>
						<nav>
							<a href="http://akblack.hcdesenvolvimentos.com.br/precisa-de-ajuda/">Precisa de ajuda? Clique aqui</a>
						</nav>
					</div>
				</div>
				<div class="col-md-3">
					<div class="menu">
						<strong>Redes sociais</strong>
						<ul class="listaRedesSociais">
							<li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
							<li><a href="#" target="_blank"><i class="fab fa-pinterest-p"></i></a></li>
						</ul>
						<div class="paginaFacebok">
							<div class="fb-page" data-href="https://www.facebook.com/AmandaKarolineOficial/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/AmandaKarolineOficial/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/AmandaKarolineOficial/">Ak</a></blockquote></div>
						</div>
					</div>
				</div>
			</div>
			<div class="fomasPagamentos">
				<span>Formas de pagamentos</span>
				<ul>
					<li><img src="http://akblack.hcdesenvolvimentos.com.br/wp-content/uploads/2018/11/americanexpress.png" alt="Bandeiras Cartões"></li>
					<li><img src="http://akblack.hcdesenvolvimentos.com.br/wp-content/uploads/2018/11/cabal.png" alt="Bandeiras Cartões"></li>
					<li><img src="http://akblack.hcdesenvolvimentos.com.br/wp-content/uploads/2018/11/dinnersclub.png" alt="Bandeiras Cartões"></li>
					<li><img src="http://akblack.hcdesenvolvimentos.com.br/wp-content/uploads/2018/11/hipercard.png" alt="Bandeiras Cartões"></li>
					<li><img src="http://akblack.hcdesenvolvimentos.com.br/wp-content/uploads/2018/11/mastercard.png" alt="Bandeiras Cartões"></li>
					<li><img src="http://akblack.hcdesenvolvimentos.com.br/wp-content/uploads/2018/11/sodexo.png" alt="Bandeiras Cartões"></li>
					<li><img src="http://akblack.hcdesenvolvimentos.com.br/wp-content/uploads/2018/11/visa.png" alt="Bandeiras Cartões"></li>
				</ul>
			</div>
		</div>
	</div>
</footer>