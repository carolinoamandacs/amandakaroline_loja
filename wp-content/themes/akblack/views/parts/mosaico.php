<!-- SESSÃO   MINI BANNERS -->
<section class="bannerCategoriaHome">
	<h6 class="hidden">Banner home</h6>
	<div class="bannersHome">

    <!-- ITEM -->
	<?php
		$miniBanners = new WP_Query( array( 'post_type' => 'miniBanner', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );
		while ( $miniBanners->have_posts() ) : $miniBanners->the_post();
			$fotoMiniBanner = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
			$fotoMiniBanner = $fotoMiniBanner[0];
			$miniBanner_link = rwmb_meta('Akablack_miniBanner_link');	
	?>
		<div class="item">
			<a href="<?php echo $miniBanner_link ?>" target="_blank">
				<figure>

					<img src="<?php echo $fotoMiniBanner ?>" alt="<?php echo get_the_title() ?>" alt="">
				</figure>
			</a>
		</div>
	<?php endwhile; wp_reset_query(); ?>


	</div>
</section>

