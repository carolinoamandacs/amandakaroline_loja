<!-- CARROSSEL DESTAQUE -->
<section class="carrosselDetaque">

	<h6 class="hidden">Carrossel de destaque</h6>

	<div id="carrosselDestaque" class="carrosselDestaque">

		<!-- ITEM -->
		<?php
			$destaques     = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );
			while ( $destaques->have_posts() ) : $destaques->the_post();
				$fotoDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$fotoDestaque = $fotoDestaque[0];
		?>
		<div class="item">
			<a href="" target="_blank">
				<figure>
					<img src="<?php echo $fotoDestaque  ?>" alt="<?php echo get_the_title() ?>">
				</figure>
			</a>
		</div>
		<?php endwhile; wp_reset_query(); ?>

	</div>

</section>