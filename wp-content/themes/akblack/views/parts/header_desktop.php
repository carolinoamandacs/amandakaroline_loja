
<?php 

global $configuracao;
global $woocommerce;
global $product;
global $current_user;
$urlMinhaConta 	= get_permalink(get_option('woocommerce_myaccount_page_id'));
$urlCarrinho    = WC()->cart->get_cart_url();
$urlCheckout 	= WC()->cart->get_checkout_url();
$urlLogout 	= wp_logout_url( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) );
// WOCOMMERCE: QUANTIDADE DE ITENS NO CARRINHO
	$qtdItensCarrinho 		= WC()->cart->cart_contents_count;
	$qtdItensCarrinhoRotulo = ($qtdItensCarrinho == 0 || $qtdItensCarrinho > 1) ? $qtdItensCarrinho . ' ' : $qtdItensCarrinho . ' '; ?>
<div class="loaderSucessoCarrinho">
	<div class="areaConteudo">
		<div class="icone">
			<i class="fas fa-cart-plus"></i>
		</div>
		<?php if ($current_user->display_name):?>
		<p><i class="fas fa-bullhorn"></i> Olá <strong><?php echo $current_user->display_name ?></strong>  seu produto foi adicionado com sucesso no carrinho!</p>
		<?php else: ?>
		<p><i class="fas fa-bullhorn"></i> Olá seu produto foi adicionado com sucesso no carrinho!</p>
		<?php endif; ?>
		<a href='<?php echo get_home_url()."/checkout/" ?>'> Clique aqui para finalizar sua compra <i class="fas fa-check-circle"></i></a>
		<span class="reload">Continuar comprando <i class="fas fa-plus"></i></span>
	</div>
</div>

<!-- TOPO -->
<header class="topo">

	<!-- MENU DESKTOP -->
	<div class="menu-desktop">
		<div class="container">
			
			<div class="areaLinksTopo">
				<div class="row">
				
					<div class="col-sm-6">
						<ul class="areaContatoTopo">			
							<li><a class="email" href="malito:<?php echo $configuracao['opt_email'] ?>"><?php echo $configuracao['opt_email'] ?></a></li>
							<li><a class="tel" href="tel:<?php echo $configuracao['opt_telefone'] ?>"><?php echo $configuracao['opt_telefone'] ?></a></li>
						</ul>
					</div>

					<div class="col-sm-6">
						<div class="areaLogin">

							<!-- VERIFICANDO SE O USUÁRIO ESTÁ LOGADO  -->
							<?php 
								if (is_user_logged_in() == true): 

									if ($current_user->display_name):
										
									
							?>
								
								<span>Olá <small class="nomeDisplay" ><?php echo $current_user->display_name; ?></small>. Acesse sua <a href="<?php echo $urlMinhaConta ?>">Conta</a> </span> 

								<?php else: ?>
								<span class="nomeDisplay">Olá <?php echo $current_user->user_login; ?>. Acesse sua <a href="<?php echo $urlMinhaConta ?>">Conta</a> ou <a href="<?php echo $urlMinhaConta ?>">Complete seu cadastro</a> </span> 
								
								<?php endif; else: ?>
									<span>Olá Visitante. Acesse sua <a href="<?php echo $urlMinhaConta ?>">Conta</a> ou <a href="<?php echo $urlMinhaConta ?>">Cadastre-se</a> </span> 
								<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<!-- MENU CONTATO -->
				<div class="col-md-3">
					
				
					<div class="logoTopo">
						<a href="<?php echo home_url('/');?>">
							<img src="<?php echo $configuracao['opt_logo']['url'] ?>" alt="<?php echo get_bloginfo() ?>">
						</a>
					</div>

				</div>

				<!-- BARRA DE PESQUISA  -->	
				<div class="col-md-6">
					<div class="search">
						<?php echo do_shortcode('[wcas-search-form]'); ?>
					</div>
				</div>

				<!-- AREA LOGIN  -->
				<div class="col-md-3">

					<!--  MENU LOGIN-->
					<div class="menuLoginTopo">
						
						<div class="areaCliente">
							
							<div class="carrinhoTopo">
								<div class="areaIcone">
									<?php if ($qtdItensCarrinhoRotulo != "0 "):?>
									<b><?php echo $qtdItensCarrinhoRotulo ?></b>
								<?php endif; ?>
									<a href="<?php echo get_home_url();echo"/cart/"?>">
										<small>Meu carrinho!</small>
										<i class="fas fa-shopping-cart"></i>
									</a>
								</div>								
							</div>
							<div class="usuarioTopo">
								<div class="areaIcone">
									<a href="<?php echo $urlMinhaConta ?>">
										<?php if ($current_user->display_name):?>
										<small><?php $current_user = explode(" ", $current_user->display_name); echo "Oi ".$current_user[0]."!"; ?></small>
										<?php else: ?>
											<small>Logar agora!</small>
										<?php endif; ?>
										<i class="fas fa-user"></i>
									</a>
								</div>
							</div>
						</div>

					</div>
				</div>
				
			</div>
		</div>
			
		<div class="menuCategoriaDesktop">
			<div class="container">
				<div class="navbar">	
					<!-- MENU MOBILE TRIGGER -->
					<button type="button" id="botao-menu" class="navbar-toggle collapsed hvr-pop" data-toggle="collapse" data-target="#collapse">
						<span class="sr-only"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<!--  MENU MOBILE-->
					<div class="row navbar-header">			
						<nav class="collapse navbar-collapse" id="collapse">
							<?php 
							$menu = array(
								'theme_location'  => '',
								'menu'            => 'Menu principal Amanda Karoline',
								'container'       => false,
								'container_class' => '',
								'container_id'    => '',
								'menu_class'      => 'nav navbar-nav',
								'menu_id'         => '',
								'echo'            => true,
								'fallback_cb'     => 'wp_page_menu',
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								'depth'           => 2,
								'walker'          => ''
							);
							wp_nav_menu( $menu );
							?>
						</nav>	
					</div>	
				</div>					
			</div>					
		</div>
	</div>	

</header>