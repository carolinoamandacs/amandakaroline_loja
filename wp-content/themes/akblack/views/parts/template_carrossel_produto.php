

<?php 


$produtosCalcas = new WP_Query( array( 
		'post_type' => 'product', 
		'orderby' => 'id', 
		'order' => 'desc', 
		'posts_per_page' => 8,
		// FILTRANDO PELA CATEGORIA DESTAQUE
		'tax_query'     => array(
			array(
				// TIPO DE CATEGORIA A SER FILTRADA / CATEGORIA DE POST
				'taxonomy' => 'product_cat',
				// PASSANDO O ATRIBUTO SLUG PARA A FILTRAGEM DO POST// PODE SER id 
				'field'    => 'slug',
				// SLUG DA CATEGORIA
				'terms'    => "calcas",
			)
		)
	) 
);

$produtosBermudas = new WP_Query( array( 
		'post_type' => 'product', 
		'orderby' => 'id', 
		'order' => 'desc', 
		'posts_per_page' => 8,
		// FILTRANDO PELA CATEGORIA DESTAQUE
		'tax_query'     => array(
			array(
				// TIPO DE CATEGORIA A SER FILTRADA / CATEGORIA DE POST
				'taxonomy' => 'product_cat',
				// PASSANDO O ATRIBUTO SLUG PARA A FILTRAGEM DO POST// PODE SER id 
				'field'    => 'slug',
				// SLUG DA CATEGORIA
				'terms'    => "roupas",
			)
		)
	) 
);
$produtosBlusas = new WP_Query( array( 
		'post_type' => 'product', 
		'orderby' => 'id', 
		'order' => 'desc', 
		'posts_per_page' => 8,
		// FILTRANDO PELA CATEGORIA DESTAQUE
		'tax_query'     => array(
			array(
				// TIPO DE CATEGORIA A SER FILTRADA / CATEGORIA DE POST
				'taxonomy' => 'product_cat',
				// PASSANDO O ATRIBUTO SLUG PARA A FILTRAGEM DO POST// PODE SER id 
				'field'    => 'slug',
				// SLUG DA CATEGORIA
				'terms'    => "blusas",
			)
		)
	) 
);
 ?>
<section class="carrosselProdutos hovercarroseis">
	<h6>Calças</h6>
 	<div id="carrosselProdutosCalcas"  class="carrosselProdutosSlick1">
	
		<?php 
			while ( $produtosCalcas->have_posts() ) : $produtosCalcas->the_post();
			global $post;
			global $product;
		
			$imagemPostDestaque  = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
			$imagemPostDestaque = $imagemPostDestaque[0];
			$product_miniatura = rwmb_meta('Akablack_product_miniatura');


			foreach ($product_miniatura as $product_miniatura) {
				$product_miniatura = $product_miniatura['full_url'];
			}
		 ?>
		<div class="item">
			
			<div class="spot">
				<a href="<?php echo get_permalink() ?>">
					<figure>
						<img src="<?php echo $imagemPostDestaque ?>" alt="<?php echo get_the_title(); ?>" data-imgFoco="<?php echo $product_miniatura ?>" data-imgPadrao="<?php echo $imagemPostDestaque ?>">
					</figure>
				</a>
				<h2><?php echo get_the_title(); ?></h2>
				<div class="precos">
					
					<?php 
						$precoOriginal = $product->price;
						$regular_price = $product->regular_price;
						$precoQuebrado = explode('.', $precoOriginal);
						$precoFormatado = $precoQuebrado[0] .','. $precoQuebrado[1];
					
					?>
					<!-- <span class="parcelamento">6x de R$10,00</span> -->
					<!-- <strong>R$60,00</strong> -->
					<?php if ($precoOriginal < $regular_price): ?>
					<strong class="promocao"><b>De <s>R$<?php echo $regular_price ?></s></b> Por R$<?php echo  $product->price; ?></strong>
					<?php else: ?>
					<strong>R$<?php echo  $product->price; ?></strong>
					<?php endif; ?>
				</div>
				<div class="btnComprar">
					<input value="Comprar" name="" class="btnComprarClique" data-productId="<?php echo $post->ID ?>" type="submit">
				</div>			
			</div>
		</div>
		<?php endwhile; wp_reset_query(); ?>

	</div> 
</section>

<section class="carrosselProdutos hovercarroseis">
	<h6>Roupas</h6>
 	<div id="carrosselProdutosBermudas"  class="carrosselProdutosSlick2">
	
		<?php 
			while ( $produtosBermudas->have_posts() ) : $produtosBermudas->the_post();
				global $product;

			$imagemPostDestaque  = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
			$imagemPostDestaque = $imagemPostDestaque[0];
			$product_miniatura = rwmb_meta('Akablack_product_miniatura');


			foreach ($product_miniatura as $product_miniatura) {
				$product_miniatura = $product_miniatura['full_url'];
			}
		 ?>
		<div class="item">
			<div class="spot">
				<a href="<?php echo get_permalink() ?>">
					<figure>
						<img src="<?php echo $imagemPostDestaque ?>" alt="<?php echo get_the_title(); ?>" data-imgFoco="<?php echo $product_miniatura ?>" data-imgPadrao="<?php echo $imagemPostDestaque ?>">
					</figure>
				</a>
				<h2><?php echo get_the_title(); ?></h2>
				<div class="precos">
					
					<?php 
						$precoOriginal = $product->price;
						$regular_price = $product->regular_price;
						$precoQuebrado = explode('.', $precoOriginal);
						$precoFormatado = $precoQuebrado[0] .','. $precoQuebrado[1];
					
					?>
					<!-- <span class="parcelamento">6x de R$10,00</span> -->
					<!-- <strong>R$60,00</strong> -->
					<?php if ($precoOriginal < $regular_price): ?>
					<strong class="promocao"><b>De <s>R$<?php echo $regular_price ?></s></b> Por R$<?php echo  $product->price; ?></strong>
					<?php else: ?>
					<strong>R$<?php echo  $product->price; ?></strong>
					<?php endif; ?>
				</div>
				<div class="btnComprar">
					<input value="Comprar" name="" class="btnComprarClique" data-productId="<?php echo $post->ID ?>" type="submit">
				</div>			
			</div>
		</div>
		<?php endwhile; wp_reset_query(); ?>

	</div> 
</section>

<section class="carrosselProdutos hovercarroseis">
	<h6>Blusas</h6>
 	<div id="carrosselProdutosBermudas"  class="carrosselProdutosSlick3">
	
		<?php 
			while ( $produtosBlusas->have_posts() ) : $produtosBlusas->the_post();
				
				global $product;
			$imagemPostDestaque  = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
			$imagemPostDestaque = $imagemPostDestaque[0];
			$product_miniatura = rwmb_meta('Akablack_product_miniatura');
		
			foreach ($product_miniatura as $product_miniatura) {
				$product_miniatura = $product_miniatura['full_url'];
			}
		 ?>
		<div class="item">
			<div class="spot">
				<a href="<?php echo get_permalink() ?>">
					<figure>
						<img src="<?php echo $imagemPostDestaque ?>" alt="<?php echo get_the_title(); ?>" data-imgFoco="<?php echo $product_miniatura ?>" data-imgPadrao="<?php echo $imagemPostDestaque ?>">
					</figure>
				</a>
				<h2><?php echo get_the_title(); ?></h2>
				<div class="precos">
					
					<?php 
						$precoOriginal = $product->price;
						$regular_price = $product->regular_price;
						$precoQuebrado = explode('.', $precoOriginal);
						$precoFormatado = $precoQuebrado[0] .','. $precoQuebrado[1];
					
					?>
					<!-- <span class="parcelamento">6x de R$10,00</span> -->
					<!-- <strong>R$60,00</strong> -->
					<?php if ($precoOriginal < $regular_price): ?>
					<strong class="promocao"><b>De <s>R$<?php echo $regular_price ?></s></b> Por R$<?php echo  $product->price; ?></strong>
					<?php else: ?>
					<strong>R$<?php echo  $product->price; ?></strong>
					<?php endif; ?>
				</div>
				<div class="btnComprar">
					<input value="Comprar" name="" class="btnComprarClique" data-productId="<?php echo $post->ID ?>" type="submit">
				</div>			
			</div>
		</div>
		<?php endwhile; wp_reset_query(); ?>

	</div> 
</section>

<section class="carrosselProdutos hovercarroseis">
	<h6>Roupas</h6>
 	<div id="carrosselProdutosBermudas"  class="carrosselProdutosSlick4">
	
		<?php 
			while ( $produtosBermudas->have_posts() ) : $produtosBermudas->the_post();
				global $product;

			$imagemPostDestaque  = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
			$imagemPostDestaque = $imagemPostDestaque[0];
			$product_miniatura = rwmb_meta('Akablack_product_miniatura');


			foreach ($product_miniatura as $product_miniatura) {
				$product_miniatura = $product_miniatura['full_url'];
			}
		 ?>
		<div class="item">
			<div class="spot">
				<a href="<?php echo get_permalink() ?>">
					<figure>
						<img src="<?php echo $imagemPostDestaque ?>" alt="<?php echo get_the_title(); ?>" data-imgFoco="<?php echo $product_miniatura ?>" data-imgPadrao="<?php echo $imagemPostDestaque ?>">
					</figure>
				</a>
				<h2><?php echo get_the_title(); ?></h2>
				<div class="precos">
					
					<?php 
						$precoOriginal = $product->price;
						$regular_price = $product->regular_price;
						$precoQuebrado = explode('.', $precoOriginal);
						$precoFormatado = $precoQuebrado[0] .','. $precoQuebrado[1];
					
					?>
					<!-- <span class="parcelamento">6x de R$10,00</span> -->
					<!-- <strong>R$60,00</strong> -->
					<?php if ($precoOriginal < $regular_price): ?>
					<strong class="promocao"><b>De <s>R$<?php echo $regular_price ?></s></b> Por R$<?php echo  $product->price; ?></strong>
					<?php else: ?>
					<strong>R$<?php echo  $product->price; ?></strong>
					<?php endif; ?>
				</div>
				<div class="btnComprar">
					<input value="Comprar" name="" class="btnComprarClique" data-productId="<?php echo $post->ID ?>" type="submit">
				</div>			
			</div>
		</div>
		<?php endwhile; wp_reset_query(); ?>

	</div> 
</section>



