<div class="fomrularioNewslletter">
	<!--START Scripts : this is the script part you can add to the header of your theme-->
	<script type="text/javascript" src="<?php echo get_home_url(); ?>/wp-includes/js/jquery/jquery.js?ver=2.10.2"></script>
	<script type="text/javascript" src="<?php echo get_home_url(); ?>/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.10.2"></script>
	<script type="text/javascript" src="<?php echo get_home_url(); ?>/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.10.2"></script>
	<script type="text/javascript" src="<?php echo get_home_url(); ?>/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.10.2"></script>
	<script type="text/javascript">
		/* <![CDATA[ */
		var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"<?php echo get_home_url(); ?>/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
		/* ]]> */
	</script><script type="text/javascript" src="<?php echo get_home_url(); ?>/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.10.2"></script>
	<!--END Scripts-->

	<div class="widget_wysija_cont html_wysija">
		<div id="msg-form-wysija-html5c5200895f29e-3" class="wysija-msg ajax"></div>
		<form id="form-wysija-html5c5200895f29e-3" method="post" action="#wysija" class="widget_wysija html_wysija">
			<input type="text" name="wysija[user][firstname]" class="wysija-input validate[required]" title="Nome" placeholder="Nome" value="" />
			<span class="abs-req">
				<input type="text" name="wysija[user][abs][firstname]" class="wysija-input validated[abs][firstname]" value="" />
			</span>
			<input type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="Email" placeholder="Email" value="" />
			<span class="abs-req">
				<input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
			</span>
			<input class="wysija-submit wysija-submit-field" type="submit" value="Enviar" />

			<input type="hidden" name="form_id" value="3" />
			<input type="hidden" name="action" value="save" />
			<input type="hidden" name="controller" value="subscribers" />
			<input type="hidden" value="1" name="wysija-page" />


			<input type="hidden" name="wysija[user_list][list_ids]" value="3" />

		</form>
	</div>
</div>