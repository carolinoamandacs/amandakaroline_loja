<?php 
	global $configuracao;
	global $woocommerce;
	global $product;
	global $current_user;
	$urlMinhaConta 	= get_permalink(get_option('woocommerce_myaccount_page_id'));
	$urlCarrinho    = WC()->cart->get_cart_url();
	$urlCheckout 	= WC()->cart->get_checkout_url();
	$urlLogout 	= wp_logout_url( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) );
	//GALERIA DE IMAGENS

 ?>
<footer class="rodape">
	<section class="newslletter">
		<h6><?php echo $configuracao['Frase_Newsllater'] ?></h6>
		
		<?php get_template_part( 'views/parts/newslletter', 'newslletter' ); ?>
		
	</section>

	<div class="divisorNewsletter">
		<img src="<?php echo get_template_directory_uri(); ?>/img/imagemArcoNewsletter.png" alt="Detalhe">
	</div>
	<div class="infoRodape">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<div class="logo">
						<a href="<?php echo home_url('/');?>">
							<img src="<?php echo $configuracao['footer_logo']['url'] ?>" alt="<?php echo get_bloginfo() ?>">
						</a>
					</div>
				</div>
				<div class="col-md-2">
					<div class="menu">
						<strong>Institucional</strong>
						<nav>
							<a href="<?php echo home_url('/quem-somos/') ?>">Quem somos</a>
							<a href="<?php echo home_url('/termos-e-condicoes/') ?>">Termos e condições</a>
							<a href="<?php echo home_url('/politica-de-privacidade/') ?>">Politica de Privacidade</a>
							<!-- <a href="<?php //echo home_url('/quero-vender/') ?>">Quero Vender!</a> -->
						</nav>
					</div>
				</div>
				<div class="col-md-2">
					<div class="menu">
						<strong>Minha conta</strong>
						<nav>
							<a href="<?php echo home_url('/minha-conta/') ?>">Meus dados</a>
							<a href="<?php echo get_home_url();echo"/my-account/orders/"?>">Meus pedidos</a>
							<a href="<?php echo $urlMinhaConta ?>">Cadastrar-se</a>
						</nav>
					</div>
				</div>
				<div class="col-md-2">
					<div class="menu">
						<strong>Atendimento</strong>
						<nav>
							<a href="<?php echo home_url('/precisa-de-ajuda/') ?>">Precisa de ajuda?</a>
							<a href="<?php echo home_url('/contato/') ?>">Contato</a>
						</nav>
					</div>
				</div>
				<div class="col-md-3">
					<div class="menu">
						<strong>Redes sociais</strong>
						<ul class="listaRedesSociais">

							<?php if ($configuracao['opt_facebook']): ?>
							<li><a href="<?php echo $configuracao['opt_facebook'] ?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
							<?php endif; ?>
							<?php if ($configuracao['opt_instagram']): ?>
							<li><a href="<?php echo $configuracao['opt_instagram'] ?>" target="_blank"><i class="fab fa-instagram"></i></a></li>
							<?php endif; ?>
							<?php if ($configuracao['opt_pinterest']): ?>
							<li><a href="<?php echo $configuracao['opt_pinterest'] ?>" target="_blank"><i class="fab fa-pinterest-p"></i></a></li>
							<?php endif; ?>
							
						</ul>
						<div class="paginaFacebok">
							<div class="fb-page" data-href="https://www.facebook.com/AmandaKarolineOficial/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/AmandaKarolineOficial/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/AmandaKarolineOficial/">Ak</a></blockquote></div>
						</div>
					</div>
				</div>
			</div>
			<div class="fomasPagamentos">
				<span>Formas de pagamentos</span>
				<ul>
					<?php
					    $myGalleryIDs = explode(',', $configuracao['opt-quem-somos-galeria']);
					    foreach($myGalleryIDs as $myPhotoID):
				    ?>
					<li><img src="<?php echo wp_get_attachment_url( $myPhotoID ); ?>" alt="Bandeiras Cartões"></li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>
	</div>
</footer>
<div class="copyright">
	<h6>Feito com <i class="fas fa-heart"></i> pela HC Desenvolvimentos</h6>
</div>
