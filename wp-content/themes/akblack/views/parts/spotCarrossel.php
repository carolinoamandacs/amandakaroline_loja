<?php 
	$imagemPostDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	$imagemPostDestaque = $imagemPostDestaque[0];
	$product_miniatura = rwmb_meta('Akablack_product_miniatura');
	global $post;
	global $product;

	foreach ($product_miniatura as $product_miniatura) {
		$product_miniatura = $product_miniatura['full_url'];
	}
 ?>
<div class="item">
	<div class="spot">
		<a href="<?php echo get_permalink() ?>">
			<figure>
				<img src="<?php echo $imagemPostDestaque ?>" alt="<?php echo get_the_title(); ?>" data-imgFoco="<?php echo $product_miniatura ?>" data-imgPadrao="<?php echo $imagemPostDestaque ?>">
			</figure>
		</a>
		<h2><?php echo get_the_title(); ?></h2>
		<div class="precos">
			<!-- <span class="parcelamento">6x de R$10,00</span> -->
			<?php 
				$precoOriginal = $product->price;
				$regular_price = $product->regular_price;
				$precoQuebrado = explode('.', $precoOriginal);
				$precoFormatado = $precoQuebrado[0] .','. $precoQuebrado[1];
			
			?>
			<!-- <span class="parcelamento">6x de R$10,00</span> -->
			<!-- <strong>R$60,00</strong> -->
			<?php if ($precoOriginal < $regular_price): ?>
			<strong class="promocao"><b>De <s>R$<?php echo $regular_price ?></s></b> Por R$<?php echo  $product->price; ?></strong>
			<?php else: ?>
			<strong>R$<?php echo  $product->price; ?></strong>
			<?php endif; ?>
		</div>
		<div class="btnComprar">
			<input value="Comprar" name="" type="submit" class="btnComprarClique" data-productId="<?php echo $post->ID ?>">
		</div>			
	</div>
</div>