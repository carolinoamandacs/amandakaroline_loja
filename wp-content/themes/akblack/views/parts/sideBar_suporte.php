<!-- SIDEBAR MENU ÁREAS PRINCIPAIS -->
<div class="col-md-3">

	<div class="sidebar">

		<ul>
			<li>
				<!-- GRUPO PRIVACIDADE -->
				<nav class="privacidade">
					<h5>INSTITUCIONAL</h5>
					<a href="<?php echo home_url('/quem-somos/') ?>">Quem somos</a>
					<a href="<?php echo home_url('/termos-e-condicoes/') ?>">Termos e condições</a>
					<a href="<?php echo home_url('/politica-de-privacidade/') ?>">Politica de Privacidade</a>
					<!-- <a href="<?php //echo get_home_url(); ?>/politica-de-privacidade">Compra segura</a> -->
				</nav>
			</li>

			<li>
				<!-- GRUPO AJUDA E SUPORTE -->
				<nav class="ajuda">
					<h5>AJUDA E SUPORTE</h5>
					<a href="<?php echo $urlMinhaConta ?>">Cadastro</a>
					<a href="<?php echo get_home_url();echo"/my-account/orders/"?>">Status do pedido</a>
					<!-- <a href="<?php echo get_home_url(); ?>/ajuda-e-suporte">Trocas e devoluções</a>
					<a href="<?php echo get_home_url(); ?>/ajuda-e-suporte">Formas de pagamento</a> -->
					<a href="<?php echo get_home_url(); ?>/contato">Contato</a>
					<a href="<?php echo get_home_url(); ?>/precisa-de-ajuda/">Precisa de Ajuda?</a>
				</nav>
			</li>

			<li>
				<!-- GRUPO DE PARCERIAS -->
				<!-- <nav class="parceria">
					<h5>PARCERIAS</h5>
					<a href="<?php echo get_home_url(); ?>/quero-vender">Quero vender!</a>
					<a href="#">Quero ser modelo!</a>
				</nav> -->
			</li>

		</ul>
	</div>
</div>