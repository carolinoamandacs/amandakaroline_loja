<?php
	/**
	 * Template Name: Inicial
	 * Description: Página inicial
	 *
	 * @package AKBLACK
	 */
	get_header();
?>
 

 <?php 
	$iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
	$ipad = strpos($_SERVER['HTTP_USER_AGENT'],"iPad");
	$android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
	$palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
	$berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
	$ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");
	$symbian =  strpos($_SERVER['HTTP_USER_AGENT'],"Symbian");

	if ($iphone || $ipad || $android || $palmpre || $ipod || $berry || $symbian == true):


	   echo get_template_part( 'views/parts/layout_mobile', 'layout_mobile' );

	?>

	<?php else: ?>
  	<div class="pg pg-inicial">
	 	<?php echo the_content(); ?>
	</div>
	<?php endif; ?>



<?php get_footer();?>