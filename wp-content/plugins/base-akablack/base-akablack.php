<?php

/**
 @wordpress-plugin
 * Plugin Name: Akablack
 * Version:     0.1
 * Plugin URI:  http://hcdesenvolvimentos.com.br 
 * Description: Controle base do tema Akablack.
 * Author:       Agência HC Desenvolvimentos
 * Author URI:   http://hcdesenvolvimentos.com.br 
 * Text Domain: hcdesenvolvimentos-base
 * Domain Path: /languages/
 * License:     GPL2
 */


function baseAkablack () {

	// TIPOS DE CONTEÚDO
	conteudosAkablack();

	taxonomiaAkablack();

	metaboxesAkablack();
}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosAkablack (){

		// TIPOS DE CONTEÚDO
		tipoDestaque();
        
        // TIPOS DE CONTEÚDO
		tipoMiniBanner();

		// TIPOS DE PARCEIROS
		tipoParceiros();

		tipoAjudaSuporte();

		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaque':
				$titulo = 'Título do destaque';
				break;

				case 'banner':
				$titulo = 'Título do banner';
				break;

				case 'team':
				$titulo = 'Enter member name here';
				break;
				
				default:
				break;
			}

			return $titulo;

		}

	}

	// CUSTOM POST TYPE DESTAQUES
	function tipoDestaque() {

		$rotulosDestaque = array(
			'name'               => 'Destaques',
			'singular_name'      => 'destaque',
			'menu_name'          => 'Destaques',
			'name_admin_bar'     => 'Destaques',
			'add_new'            => 'Adicionar novo',
			'add_new_item'       => 'Adicionar novo destaque',
			'new_item'           => 'Novo destaque',
			'edit_item'          => 'Editar destaque',
			'view_item'          => 'Ver destaque',
			'all_items'          => 'Todos os destaques',
			'search_items'       => 'Buscar destaque',
			'parent_item_colon'  => 'Dos destaques',
			'not_found'          => 'Nenhum destaque cadastrado.',
			'not_found_in_trash' => 'Nenhum destaque na lixeira.'
		);

		$argsDestaque 	= array(
			'labels'             => $rotulosDestaque,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-megaphone',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'destaque' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','thumbnail')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('destaque', $argsDestaque);

	}
    
    // CUSTOM POST TYPE MINI BANNERS PG INICIAL
	function tipoMiniBanner() {

		$rotulosMiniBanner = array(
			'name'               => 'Mini Banners',
			'singular_name'      => 'banner',
			'menu_name'          => 'Mini Banners',
			'name_admin_bar'     => 'Mini Banners',
			'add_new'            => 'Adicionar novo',
			'add_new_item'       => 'Adicionar novo banner',
			'new_item'           => 'Novo banner',
			'edit_item'          => 'Editar banner',
			'view_item'          => 'Ver banner',
			'all_items'          => 'Todos os mini banners',
			'search_items'       => 'Buscar banner',
			'parent_item_colon'  => 'Dos banners',
			'not_found'          => 'Nenhum banner cadastrado.',
			'not_found_in_trash' => 'Nenhum banner na lixeira.'
		);

		$argsMiniBanner 	= array(
			'labels'             => $rotulosMiniBanner,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-megaphone',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'miniBanner' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','thumbnail')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('miniBanner', $argsMiniBanner);

	}

	// CUSTOM POST TYPE PARCEIROS
	function tipoParceiros() {

		$rotulosParceiros = array(
			'name'               => 'Parceiros',
			'singular_name'      => 'parceiro',
			'menu_name'          => 'Parceiros',
			'name_admin_bar'     => 'Parceiros',
			'add_new'            => 'Adicionar novo',
			'add_new_item'       => 'Adicionar novo parceiro',
			'new_item'           => 'Novo parceiro',
			'edit_item'          => 'Editar parceiro',
			'view_item'          => 'Ver parceiro',
			'all_items'          => 'Todos os parceiros',
			'search_items'       => 'Buscar parceiro',
			'parent_item_colon'  => 'Dos parceiros',
			'not_found'          => 'Nenhum parceiro cadastrado.',
			'not_found_in_trash' => 'Nenhum parceiro na lixeira.'
		);

		$argsParceiros 	= array(
			'labels'             => $rotulosParceiros,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-groups',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'parceiros' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','thumbnail')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('parceiro', $argsParceiros);

	}

	function tipoAjudaSuporte(){
		$rotulosSuporte = array(
			'name'               => 'Suporte',
			'singular_name'      => 'suporte',
			'menu_name'          => 'Ajuda e Suporte',
			'name_admin_bar'     => 'Ajuda e Suporte',
			'add_new'            => 'Adicionar novo',
			'add_new_item'       => 'Adicionar novo suporte',
			'new_item'           => 'Novo suporte',
			'edit_item'          => 'Editar suporte',
			'view_item'          => 'Ver suporte',
			'all_items'          => 'Todos os suportes',
			'search_items'       => 'Buscar suporte',
			'parent_item_colon'  => 'Dos suportes',
			'not_found'          => 'Nenhum suporte cadastrado.',
			'not_found_in_trash' => 'Nenhum suporte na lixeira.'
		);

		$argsSuporte 	= array(
			'labels'             => $rotulosSuporte,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-editor-help',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'ajuda-e-suporte' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','editor')
		);
		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('suporte', $argsSuporte);
	}
	

	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaAkablack () {		
		//taxonomiaCategoriaDestaque();
	}
		// TAXONOMIA DE DESTAQUE
	function taxonomiaCategoriaDestaque() {

		$rotulosCategoriaDestaque = array(
			'name'              => 'Categorias de destaque',
			'singular_name'     => 'Categoria de destaque',
			'search_items'      => 'Buscar categorias de destaque',
			'all_items'         => 'Todas as categorias de destaque',
			'parent_item'       => 'Categoria de destaque pai',
			'parent_item_colon' => 'Categoria de destaque pai:',
			'edit_item'         => 'Editar categoria de destaque',
			'update_item'       => 'Atualizar categoria de destaque',
			'add_new_item'      => 'Nova categoria de destaque',
			'new_item_name'     => 'Nova categoria',
			'menu_name'         => 'Categorias de destaque',
		);

		$argsCategoriaDestaque 		= array(
			'hierarchical'      => true,
			'labels'            => $rotulosCategoriaDestaque,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'categoria-destaque' ),
		);

		register_taxonomy( 'categoriaDestaque', array( 'destaque' ), $argsCategoriaDestaque );

	}			

    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesAkablack(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

	function registraMetaboxes( $metaboxes ){

		$prefix = 'Akablack_';

		// METABOX DE PARCEIRO
		$metaboxes[] = array(

			'id'			=> 'detailsMetaboxParceiro',
			'title'			=> 'Parceiro',
			'pages' 		=> array( 'parceiro' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(

				array(
					'name'  => 'Parceiro:',
					'id'    => "{$prefix}parceiro_link",
					'desc'  => 'Link parceiro',
					'type'  => 'text',
				),
			),
		);		

		// METABOX DE BANNER PG INICIAL
		$metaboxes[] = array(

			'id'			=> 'detailsMetaboxBanner',
			'title'			=> 'Mini Banner Pagina Inicial',
			'pages' 		=> array( 'miniBanner' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(

				array(
					'name'  => 'Link do Banner:',
					'id'    => "{$prefix}miniBanner_link",
					'desc'  => 'Insira o link',
					'type'  => 'text',
				),
				
				
			),
		);

		
		/************************
		*	METABOX DE POST
		*************************/
		$metaboxes[] = array(

			'id'			=> 'detalhesProduct',
			'title'			=> 'Detalhes do produto',
			'pages' 		=> array( 'product' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(

				array(
					'name'  => 'Foto : ',
					'id'    => "{$prefix}product_miniatura",
					'desc'  => '',
					'type'  		   => 'image_advanced',
					'max_file_uploads' => 1
				),
				array(
					'name'  => 'Desconto: ',
					'id'    => "{$prefix}desconto",
					'desc'  => '',
					'type'  => 'text',
				),
				array(
					'name'  => 'Produto novo? ',
					'id'    => "{$prefix}produto_novo",
					'desc'  => '',
					'type'  => 'checkbox',
				),
			)
		);


		return $metaboxes;
	}

	function metaboxjs(){

		global $post;
		$template = get_post_meta($post->ID, '_wp_page_template', true);
		$template = explode('/', $template);
		$template = explode('.', $template[1]);
		$template = $template[0];

		if($template != ''){
			wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
		}
	}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesAkablack(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerAkablack(){

		if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseAkablack');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

		baseAkablack();

		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );